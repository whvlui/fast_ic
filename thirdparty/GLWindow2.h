// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GLWINDOW_2_H_
#define GLWINDOW_2_H_

#include <cvd/glwindow.h>
#include <cvd/gl_helpers.h>
#include <cvd/image.h>
#include <TooN/TooN.h>

class GLWindow2 : public CVD::GLWindow, public CVD::GLWindow::EventHandler
{
public:
	GLWindow2(CVD::ImageRef irSize, std::string sTitle);
	GLWindow2(CVD::ImageRef irSize, std::string sTitle, std::string sObj);

	// Some OpenGL helpers
	void SetupViewPort(const CVD::ImageRef irPos, const CVD::ImageRef irSize);
	void SetupWindowOrtho(const TooN::Vector<4> &v4);
	void SetupWindowPerspective(const TooN::Vector<4> &v4);

	// Event handlers
	void HandlePendingEvents();
	virtual void on_key_down(GLWindow&, int);
	virtual void on_mouse_down(GLWindow&, CVD::ImageRef, int, int);
	virtual void on_mouse_move(GLWindow&, CVD::ImageRef, int);

    // Common drawing functions
    template <class T>
    void DrawCVDImage(CVD::ImageRef irVPpos, CVD::Image<T> &im)
    {
        SetupViewPort(irVPpos,im.size());
        SetupWindowOrtho(TooN::makeVector(0,im.size().x,im.size().y,0));
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glRasterPos2d(0,0);
        glDrawPixels(im);
    }

	void PrintString(CVD::ImageRef irPos, std::string s, int nSize);

	// Graph viewer functions
	std::pair<TooN::Vector<6>, TooN::Vector<6> > GetMousePoseUpdate();

private:
	std::string sClass;
	CVD::ImageRef mirPreviousMousePos;

	TooN::Vector<6> mvLeftPoseUpdate;
	TooN::Vector<6> mvMCPoseUpdate;
};


#endif /* GLWINDOW_2_H_ */
