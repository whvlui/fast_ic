// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "GLWindow2.h"
#include <cvd/gl_helpers.h>
#include <gvars3/instances.h>
#include <string>
#include <sstream>

using namespace GVars3;
using namespace CVD;
using namespace TooN;
using namespace std;

GLWindow2::GLWindow2(ImageRef irSize, string sTitle)
  : GLWindow(irSize, sTitle)
{
	mvLeftPoseUpdate = Zeros;
	mvMCPoseUpdate = Zeros;
}

GLWindow2::GLWindow2(CVD::ImageRef irSize, std::string sTitle, std::string sObj)
	: GLWindow(irSize, sTitle)
{
	sClass = sObj;
	mvLeftPoseUpdate = Zeros;
	mvMCPoseUpdate = Zeros;
}

void GLWindow2::SetupViewPort(const CVD::ImageRef irPos, const CVD::ImageRef irSize)
{
	glViewport(irPos.x,irPos.y,irSize.x,irSize.y);
}

void GLWindow2::SetupWindowOrtho(const Vector<4> &v4)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(v4[0], v4[1], v4[2], v4[3], -100, 100);
}

void GLWindow2::SetupWindowPerspective(const TooN::Vector<4> &v4)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(v4[0], v4[1], v4[2], v4[3]);
}

void GLWindow2::HandlePendingEvents()
{
	handle_events(*this);
}

void GLWindow2::on_key_down(GLWindow&, int k)
{
	string s;
	if( (k >= 48 && k <=57) || ( k >=97 && k <= 122) || (k >= 65 && k <= 90)){
		char c = k;
		if(c >= 65 && c <= 90)
			c = c + 32;
		s = c;
	}
	else switch (k) // Some special chars are translated:
	{
	case 33: s="PageUp"; break;
	case 34: s="PageDown"; break;
	case 13: s="Enter"; break;
	case 32:  s="Space"; break;
	case 8:  s="BackSpace"; break;
	case 27:  s="Escape"; break;
	default: break;
	}


	if(s != ""){
		GUI.ParseLine("try KeyPress " + s);
	}
}

void GLWindow2::on_mouse_down(GLWindow&, CVD::ImageRef imCursorPos, int state, int button){
	int x = imCursorPos.x;
	int y = imCursorPos.y;
	stringstream ss;
	ss << x << " " << y;
	if(button == MouseButton::BUTTON_LEFT)
		ss << " " << "left";
	else if(button == MouseButton::BUTTON_RIGHT)
		ss << " " << "right";
	string s = ss.str();

	if(s != ""){
		GUI.ParseLine("try " + sClass + ".MouseDown " + s);
	}
}

void GLWindow2::on_mouse_move(GLWindow&, CVD::ImageRef where, int state)
{
	ImageRef irMotion = where - mirPreviousMousePos;
	mirPreviousMousePos = where;

	double dSensitivity = 0.01;
	if((state & BUTTON_LEFT) && ! (state & BUTTON_RIGHT))
	{
		mvMCPoseUpdate[3] -= irMotion[1] * dSensitivity;
		mvMCPoseUpdate[4] += irMotion[0] * dSensitivity;
	}
	if(!(state & BUTTON_LEFT) && (state & BUTTON_RIGHT))
	{
		mvLeftPoseUpdate[4] -= irMotion[0] * dSensitivity;
		mvLeftPoseUpdate[3] += irMotion[1] * dSensitivity;
	}
    else if(/*(state & BUTTON_MIDDLE)  ||*/ ((state & BUTTON_LEFT) && (state & BUTTON_RIGHT)))
	{
		mvLeftPoseUpdate[5] -= irMotion[0] * dSensitivity;
		mvLeftPoseUpdate[2] += irMotion[1] * dSensitivity;
	}
}

std::pair<TooN::Vector<6>, TooN::Vector<6> > GLWindow2::GetMousePoseUpdate()
{
	pair<Vector<6>, Vector<6> > result;
	result.first = mvLeftPoseUpdate;
	result.second = mvMCPoseUpdate;
	mvLeftPoseUpdate = Zeros;
	mvMCPoseUpdate = Zeros;

	return result;
}

//template<typename T>
//void GLWindow2::DrawCVDImage(CVD::ImageRef irVPpos, CVD::Image<T> &im)
//{
//    SetupViewPort(irVPpos,im.size());
//    SetupWindowOrtho(makeVector(0,im.size().x,im.size().y,0));
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//    glRasterPos2d(0,0);
//    glDrawPixels(im);
//}

void GLWindow2::PrintString(CVD::ImageRef irPos, std::string s, int nSize)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glTranslatef(irPos.x,irPos.y,0.0);
	glScalef(nSize,-nSize,1);
	glDrawText(s, NICE, 1.6, 0.1);
	glPopMatrix();
}
