#!/usr/bin/env python
# Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
#
# This file is part of FastIC.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Simple script to plot convergence results for 2D image alignment")
parser.add_argument('file', help='results text file (format: time, pixel RMSE in the order of IC, re-weighted IC, diag, scaled identity, off diag)')
args = parser.parse_args()

# (1) Parse results file
file = open(args.file, "r")

per_row = []
for line in file:
        rows = [float(x) for x in line.strip().split(' ')]
        per_row.append(rows)

file.close()

per_column = zip(*per_row)

# Plot results
plt.figure(1)
plt.plot(per_column[0], per_column[1], 'g', label = 'IC', linewidth=1.5)
plt.plot(per_column[2], per_column[3], 'k', label = 're-weighted IC', linewidth=1.5)
plt.plot(per_column[4], per_column[5], 'c', label = 'diag', linewidth=1.5)
plt.plot(per_column[8], per_column[9], 'm', label = 'off-diag', linewidth=1.5)
plt.plot(per_column[10], per_column[11], 'b', label = 'scaled identity', linewidth=1.5)
plt.ylabel('Pixel RMSE')
plt.xlabel('Time (ms)')

plt.show();
