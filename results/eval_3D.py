#!/usr/bin/python
# Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
#
# This file is part of FastIC.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
import argparse
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description="Simple script to plot convergence results for RGB-D image alignment")
parser.add_argument('file', help='results text file (format: time, translational RMSE, rotational RMSE in the order of IC, re-weighted IC, diag, scaled identity, off diag)')
args = parser.parse_args()

# (1) Parse results file
file = open(args.file, "r")

per_row = []
for line in file:
	rows = [float(x) for x in line.strip().split(' ')]
	per_row.append(rows)

file.close()
	
per_column = zip(*per_row)

# Convert lists to numpy arrays
cols = np.array(per_column);

# Plot results
mTomm = 1000;
plt.figure(1)
plt.subplot(211)
plt.plot(per_column[0], [y*mTomm for y in per_column[1]], 'g', label = 'IC', linewidth=1.5)
plt.plot(per_column[3], [y*mTomm for y in per_column[4]], 'k', label = 're-weighted IC', linewidth=1.5)
plt.plot(per_column[6], [y*mTomm for y in per_column[7]], 'c', label = 'diag', linewidth=1.5)
plt.plot(per_column[12], [y*mTomm for y in per_column[13]], 'm', label = 'off-diag', linewidth=1.5)
plt.plot(per_column[15], [y*mTomm for y in per_column[16]], 'b', label = 'scaled identity', linewidth=1.5)

plt.legend(bbox_to_anchor=(0., 1.02, 1. ,.102), loc=3, ncol=5, mode="expand", borderaxespad=0., fontsize="small")
plt.ylabel('Translational RMSE (mm)')
plt.xlabel('Time (ms)')
plt.xlim([0,40])

rad_to_deg = 180.0/math.pi;
plt.subplot(212)
plt.plot(per_column[0], [y*rad_to_deg for y in per_column[2]], 'g',
	 per_column[3], [y*rad_to_deg for y in per_column[5]], 'k',
	 per_column[6], [y*rad_to_deg for y in per_column[8]], 'c',
	 per_column[12], [y*rad_to_deg for y in per_column[14]], 'b',
	 per_column[15], [y*rad_to_deg for y in per_column[17]], 'm',
	 linewidth=1.5)
plt.ylabel('Rotational RMSE ($\circ$)')
plt.xlabel('Time (ms)')
plt.xlim([0,40])

plt.show()
