### Fast IC ###

This repository contains a C++ implementation of a fast inverse compositional image alignment algorithm which does not require the Jacobian and Hessian to be re-computed at every iteration while remaining robust to outliers and missing data. More information regarding this work can be found in our BMVC 2015 paper. If you find the code useful, please cite:

* **Fast Inverse Compositional Image Alignment with Missing Data and Reweighting**, *V. Lui, D. Gamage, T. Drummond*, BMVC 2015.

## 1. Overview

This repository's source code can be divided into the folders **core** and **benchmark**. The folder ** core ** contains code to perform image alignment using either canonical inverse composition, efficient second-order minimization (ESM), or inverse composition with preconditioning on three different motion models: affine warps, homographies, or SE(3) warps. The folder **benchmark** contains code to evaluate the convergence of each algorithm using either simulated data (**simulation** folder) or real data(**RGBD_img_seq** folder). Currently, the real data experiment has only been tested on SE(3) warps with RGB-D data. The other folders in this repository are:

*  **results**: stores results from experiments and contains python scripts to produce convergence plots.
* **cfg**: stores parameters for experiments
* **thirdparty** : stores some source code from PTAM

## 2. Installation

FastIC has been tested using Ubuntu 14.04 (trusty) and has three main dependencies:

* TooN - a header library for linear algebra
* libCVD - a lightweight computer vision library
* GVars3 - a run-time configuration library

All three libraries are written by members of the Cambridge Machine Intelligence lab and are licensed under the LGPL license. To install the dependencies for these libraries, type the following command in the terminal:

    sudo apt-get install liblapack-dev libblas-dev libjpeg-dev libpng-dev libtiff-dev

The libraries should be installed in the following order: 1. TooN, 2. libCVD, 3. GVars3.

For TooN,

    git clone git://github.com/edrosten/TooN.git
    cd /path/to/TooN/folder/
    ./configure
    make
    sudo make install

For libCVD,

    git clone git://github.com/edrosten/libcvd.git
    cd /path/to/libCVD/folder/
    ./configure --without-ffmpeg
    make
    sudo make install

For GVars3,

    https://github.com/edrosten/gvars.git
    cd /path/to/GVars3/folder/
    ./configure --disable-widgets
    make
    sudo make install

Finally, to install FastIC,

    git clone https://whvlui@bitbucket.org/whvlui/fast_ic.git
    cd /path/to/FastIC/folder
    mkdir build
    cd build
    cmake ..
    make

## 3. Usage
There are two main pieces of code in the **core** folder. *AlignAlg.h* stores the different image alignment algorithms, whereas the classes *AlignmentAff*, *AlignmentRGBD*, *AlignmentHomo* provide an interface for the image alignment algorithms to store and access the required data.

### 3.1 Preparation
In the project folder, create a data folder:

    mkdir data

### 3.2 Aligning a pair of images
 To instantiate an Alignment object, you need to provide the template and reference images to the object constructor. This is then followed by a call to the Align member function in the Alignment object. Examples can be found in *test_aff.cpp* and *test_homo.cpp*.

### 3.3 Evaluating algorithms through simulation
The executable *sim_experiment* allows one to evaluate the different image alignment algorithms using synthetic warps. First, place the input image(s) to be used in the data folder. For affine warps and homographies, the input image is simply an RGB image. For SE(3) warps, the required input is an RGB image and its corresponding depth image.

The parameters for the experiment can be changed in *cfg/sim_params.cfg* This allows one to specify the path to the input images, as well as playing around with the amount of noise and outliers to be added to the data.
*sim_experiment* takes a single char[] argument, which can either be "affine", "homography" or "se3". For example, to run a simulation using affine warps:

    sim_experiment affine

### 3.4 Evaluating SE(3) warps with real data

The executable *real_experiment* allows one to evaluate the different image alignment algorithms using an image sequence. We have tested our algorithm on the publicly available TUM RGB-D dataset [http://vision.in.tum.de/data/datasets/rgbd-dataset]. Here, we assume that there is a folder RGB/ which stores the colour images and a depth/ folder which stores the depth images, which is how it's done in the TUM RGB-D and ICL-NUIM dataset. To run the experiment, perform the following steps:

1. Download one of the TUM RGB-D datasets, for example the freiburg1-xyz dataset.
2. Download the *associate.py* script from the TUM RGB-D dataset website.
3. Using *associate.py*, create mapping from rgb image to ground truth:

        python associate.py rgb.txt groundtruth.txt > rgb_gt.txt

4. Remove some columns from rgb_gt.txt:
        
        cut -d " " -f1,4-10 rgb_gt.txt > sync_rgb_gt.txt

5. Using *associate.py*, create mapping from rgb image to depth image:

        python associate.py rgb.txt depth.txt > rgb_to_d.txt

7. Remove some columns from rgb_to_d.txt:

        cut -d " " -f1,3 rgb_to_d.txt > sync_rgb_to_d.txt

8. cd to the data folder.
9. Create a soft symbolic link to where the dataset is stored

        ln -s /path/to/dataset/ [dataset]

    Change the value of real_params.data_folder in cfg/real_params.cfg accordingly. For example, if [dataset]     is named as dataset_frei1, then real_params.data_folder = "../data/dataset_frei1/". 

10. Run real_experiment

# 3.5 Visual Odometry example

Assuming that you have set up the dataset folder and the data folder as described in Section 3.4, the executable *vis_odometry* is an example visual odometry application for consecutive pairs of images in the dataset. Once you have run *vis_odometry*, you can use *visualizer* to visualize the results.