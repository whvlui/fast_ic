// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cvd/image_io.h>
#include <cvd/random.h>
#include <cvd/image_interpolate.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include "../../core/AlignmentHomo.h"

TooN::Matrix<3>
GenHomoWarp()
{
    // Input points
    std::vector<TooN::Vector<3> > cam1;
    cam1.push_back(TooN::makeVector(0,0,1));
    cam1.push_back(TooN::makeVector(99,0,1));
    cam1.push_back(TooN::makeVector(0,99,1));
    cam1.push_back(TooN::makeVector(99,99,1));

    // Perturbed points
    std::vector<TooN::Vector<3> > cam2;
    for(size_t i=0; i < cam1.size(); ++i)
    {
        TooN::Vector<3> v3 = TooN::Zeros;
        v3[2] = 1.0;
        for(int j=0; j<2; ++j)
            v3[j] = cam1[i][j] + 3.0*CVD::rand_g();

        cam2.push_back(v3);
    }

    // Compute action matrix A
    TooN::Matrix<9> A = TooN::Zeros;
    for(size_t pt = 0; pt < cam1.size(); ++pt)
    {
        A[pt*2+0][0] = cam1[pt][0];
        A[pt*2+0][1] = cam1[pt][1];
        A[pt*2+0][2] = 1;
        A[pt*2+0][3] = 0;
        A[pt*2+0][4] = 0;
        A[pt*2+0][5] = 0;
        A[pt*2+0][6] = -cam1[pt][0]*cam2[pt][0];
        A[pt*2+0][7] = -cam1[pt][1]*cam2[pt][0];
        A[pt*2+0][8] = -cam2[pt][0];

        A[pt*2+1][0] = 0;
        A[pt*2+1][1] = 0;
        A[pt*2+1][2] = 0;
        A[pt*2+1][3] = cam1[pt][0];
        A[pt*2+1][4] = cam1[pt][1];
        A[pt*2+1][5] = 1;
        A[pt*2+1][6] = -cam1[pt][0]*cam2[pt][1];
        A[pt*2+1][7] = -cam1[pt][1]*cam2[pt][1];
        A[pt*2+1][8] = -cam2[pt][1];
    }

    for(int i=0; i<9; ++i)
        A[8][i] = 0.0;

    // Get homography
    TooN::SVD<> svdH(A);
    TooN::Vector<9> Hvec = svdH.get_VT()[8];

    TooN::Matrix<3> H = TooN::Zeros;
    H[0] = Hvec.slice<0,3>();
    H[1] = Hvec.slice<3,3>();
    H[2] = Hvec.slice<6,3>();

    H = H * (1.0 / H(2,2));

    return H;

}

int main()
{
    std::cout << "Image alignment test with homographies" << std::endl;

    CVD::Image<CVD::byte> im = CVD::img_load("../data/takeo.pgm");
    CVD::Image<CVD::byte> im_ref;
    im_ref.resize(CVD::ImageRef(100,100));
    im_ref.fill(0);
    CVD::ImageRef irScan;
    do
    {
        CVD::ImageRef irTarget = irScan + CVD::ImageRef(25,62);
        im_ref[irScan] = im[irTarget];
    }while(irScan.next(im_ref.size()));

    TooN::Matrix<3> m3H = GenHomoWarp();
    TooN::SVD<3> svdH(m3H);
    TooN::Matrix<3> m3HInv = svdH.get_pinv();

    CVD::Image<CVD::byte> im_live;
    im_live.resize(CVD::ImageRef(100,100));
    im_live.fill(0);

    irScan = CVD::ImageRef(0,0);
    CVD::image_interpolate<CVD::Interpolate::Bilinear,CVD::byte> interp(im_ref);
    do
    {
        TooN::Vector<3> v3 = m3HInv * TooN::makeVector(irScan.x,irScan.y,1);
        TooN::Vector<2> v2 = TooN::project(v3);

        if(v2[0] < 0 || v2[1] < 0
                || v2[0] > im_ref.size().x
                || v2[1] > im_ref.size().y)
            continue;
        im_live[irScan] = interp[v2];

    }while(irScan.next(im_ref.size()));

    AlignmentHomo homo_solver(im_ref, im_live);
    homo_solver.Align("diagonal",true);

    return 0;
}
