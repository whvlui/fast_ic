// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ExpSimBase.h"
#include "ExpSimAff.h"
#include "ExpSimHomo.h"
#include "ExpSimRGBD.h"
#include <cvd/random.h>
#include <cstdlib>

ExpSimBase::ExpSimBase()
{
}

ExpSimBase*
ExpSimBase::Create(std::string s)
{
    if(s == "affine")
        return new ExpSimAff();
    else if(s=="homography")
        return new ExpSimHomo();
    else if(s=="se3")
        return new ExpSimRGBD();
}

void
ExpSimBase::AddGaussianNoise(CVD::Image<CVD::byte> &im, double dSigma)
{
    CVD::ImageRef irScan;
    do
    {
        double dIntensity = (double) im[irScan] + (CVD::rand_g()*dSigma);
        if(dIntensity > 255.0)
            dIntensity = 255.0;
        else if(dIntensity < 0.0)
            dIntensity = 0.0;

        im[irScan] = (CVD::byte) dIntensity;

    }while(irScan.next(im.size()));
}

void
ExpSimBase::AddOutliers(CVD::Image<CVD::byte> &im, double dRatio)
{
    int num_outlier_pixels = dRatio*im.size().x*im.size().y;
    int nDim = sqrt(num_outlier_pixels);
    int nDimPlusOne = nDim+1;
    int nDimMinusOne = nDim-1;
    double nDimPlusOneSq = nDimPlusOne*nDimPlusOne;
    double nDimMinusOneSq = nDimMinusOne*nDimMinusOne;

    double dErr_nDimPlusOne = fabs(nDimPlusOneSq - num_outlier_pixels);
    double dErr_nDimMinusOne = fabs(nDimMinusOneSq - num_outlier_pixels);
    int nSqSize = dErr_nDimPlusOne < dErr_nDimMinusOne ? nDimPlusOne : nDimMinusOne;

    int nX = drand48() * (im.size().x - nSqSize);
    int nY = drand48() * (im.size().y - nSqSize);
    for(int r = nX; r < nX + nSqSize; ++r)
    {
        for(int c = nY; c < nY + nSqSize; ++c)
        {
            CVD::ImageRef irPos = CVD::ImageRef(r,c);
            im[irPos] = drand48() * 255.0;
        }
    }
}

void
ExpSimBase::CumSum(std::vector<double> &vec)
{
    double dCumSum=0.0;
    for(size_t i=0; i<vec.size(); ++i)
    {
        dCumSum += vec[i];
        vec[i] = dCumSum;
    }
}
