// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ExpSimRGBD.h"
#include <gvars3/GStringUtil.h>
#include <gvars3/instances.h>
#include <cvd/image_io.h>
#include <cvd/image_interpolate.h>
#include <cvd/random.h>
#include <sstream>
#include <fstream>
#include <cmath>

using namespace std;
using namespace CVD;
using namespace TooN;
using namespace GVars3;

ExpSimRGBD::ExpSimRGBD()
{
    // Load RGB-D image
    LoadTemplate();

    // Load camera params
    double cam_fx = GV3::get<double>("rgbd.cam_fx",600);
    double cam_fy = GV3::get<double>("rgbd.cam_fy",600);
    double cam_px = GV3::get<double>("rgbd.cam_px",320);
    double cam_py = GV3::get<double>("rgbd.cam_py",240);

    cam_params.get_parameters() = TooN::makeVector(cam_fx,cam_fy,cam_px,cam_py);

    dTmpNoiseSigma = GVars3::GV3::get<double>("rgbd.tmp_noise_sigma",0.0);
    dRefNoiseSigma = GVars3::GV3::get<double>("rgbd.ref_noise_sigma",0.0);
    dRefOutlierRatio = GVars3::GV3::get<double>("rgbd.ref_outlier_ratio",0.0);
    assert(dRefOutlierRatio < 1.0);

    mMethodToStats["default"] = Stats();
    mMethodToStats["scaled_identity"] = Stats();
    mMethodToStats["diagonal"] = Stats();
    mMethodToStats["off_diagonal"] = Stats();
    mMethodToStats["esm"] = Stats();
    mMethodToStats["default_reweight"] = Stats();
}

void
ExpSimRGBD::LoadTemplate()
{
    std::string sRGB = GV3::get<std::string>("rgbd.rgb","");
    std::string sDepth = GV3::get<std::string>("rgbd.depth","");

    imB = img_load(sRGB);
    im_depth = img_load(sDepth);

    im_tmp = imB;
}

void
ExpSimRGBD::Reset()
{
    im_tmp = imB;
    im_ref.fill(0);
}

void
ExpSimRGBD::GenerateWarp()
{
    // Generate random translation vector,
    // sampled from a uniform distribution of +/- 0.01m
    double tx = 0, ty = 0, tz = 0;
    switch(rand() % 3)
    {
    case 0:
        tx = drand48() * 0.02 - 0.01;
        break;
    case 1:
        ty = drand48() * 0.02 - 0.01;
        break;
    case 2:
        tz = drand48() * 0.02 - 0.01;
        break;
    }

    // Generate random rotation
    double rx = 0, ry = 0, rz = 0;
    switch(rand() % 3)
    {
    case 0:
        rx = drand48() * 0.04 - 0.02;
        break;
    case 1:
        ry = drand48() * 0.04 - 0.02;
        break;
    case 2:
        rz = drand48() * 0.04 - 0.02;
        break;
    }

    TooN::Vector<6> v6 = TooN::makeVector(tx,ty,tz,rx,ry,rz);
    se3 = TooN::SE3<>::exp(v6);
}

void
ExpSimRGBD::CreateReferenceImage()
{
    im_ref.resize(im_tmp.size());
    im_ref.fill(0);

    CVD::ImageRef irPos;
    CVD::image_interpolate<CVD::Interpolate::Bilinear,CVD::byte> interp(im_tmp);
    do
    {
        const TooN::Vector<4> v4 = cam_params.get_parameters();
        double Z = (double) im_depth[irPos] / 5000.0;
        double X = (irPos.x - v4[2]) * Z / v4[0];
        double Y = (irPos.y - v4[3]) * Z / v4[1];
        TooN::Vector<3> v3 = TooN::makeVector(X,Y,Z);

        if(v3[0]==0 && v3[1]==0 && v3[2]==0)
            continue;

        TooN::Vector<2> v2Im = cam_params.project(project(se3*v3));
        if(interp.in_image(v2Im)==false)
            continue;

        im_ref[irPos] = interp[v2Im];

    }while(irPos.next(im_tmp.size()));
}

void
ExpSimRGBD::DumpResults()
{
    for(std::map<std::string,Stats>::iterator
        it = mMethodToStats.begin();
        it != mMethodToStats.end();
        ++it)
    {
        // Compute average:
        Stats &stats = it->second;
        for(size_t i=0; i<stats.vdTime.size();++i)
        {
            stats.vdTime[i] /= nWarps;
            stats.vdTransErr[i] /= nWarps;
            stats.vdRotErr[i] /= nWarps;
        }
        CumSum(stats.vdTime);
    }

    std::ofstream outFile;
    std::string sOut = GVars3::GV3::get<std::string>("rgbd.results","");
    outFile.open(sOut);

    int n = mMethodToStats.at("default").vdTime.size();
    for(int i = 0; i < n; ++i)
    {
        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            Stats& stats = it->second;
            outFile << stats.vdTime[i] << " ";
            outFile << stats.vdTransErr[i] << " ";
            outFile << stats.vdRotErr[i] << " ";
        }
        outFile << std::endl;
    }
    outFile.close();
}

void
ExpSimRGBD::RunNWarps(int n)
{
    nWarps = n;
    for(int i=0; i<n; ++i)
    {
        std::cout << "Crunching image " << i << std::endl;

        Reset();
        GenerateWarp();
        CreateReferenceImage();
        AddGaussianNoise(im_tmp, dTmpNoiseSigma);
        AddGaussianNoise(im_ref, dRefNoiseSigma);
        AddOutliers(im_ref, dRefOutlierRatio);

        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            std::string sMethod = it->first;
            RunOneMethod(sMethod);
        }
    }
}

void
ExpSimRGBD::RunOneMethod(std::string s)
{
    bool bReweight = true;
    std::string sMethod = s;
    if(s == "default_reweight")
        sMethod = "default";
    if(s == "default")
        bReweight = false;

    AlignmentRGBD RGBDsolver(im_tmp, im_depth, im_ref);
    RGBDsolver.Align(sMethod, bReweight);

    Result result = RGBDsolver.DumpResult();
    Stats &stats = mMethodToStats.at(s);
    if(stats.vdTime.size() == 0)
    {
        stats.vdTime.assign(result.vdTime.size(), 0.0);
        stats.vdTransErr.assign(result.vdTime.size(), 0.0);
        stats.vdRotErr.assign(result.vdTime.size(), 0.0);
    }

    for(size_t i = 0; i < stats.vdTime.size(); ++i)
    {
        SE3<> se3_err = se3 * result.se3_vec[i];
        double dTransErr = TooN::norm(se3_err.get_translation());
        double dRotErr = TooN::norm(se3_err.get_rotation().ln());

        stats.vdTransErr[i] += dTransErr;
        stats.vdRotErr[i] += dRotErr;
        stats.vdTime[i] += result.vdTime[i];
    }
}
