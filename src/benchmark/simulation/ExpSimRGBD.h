// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXPSIMRGBD_H
#define EXPSIMRGBD_H

#include <map>
#include <vector>
#include <string>
#include <cvd/image.h>
#include <cvd/byte.h>
#include <cvd/rgb.h>
#include <cvd/camera.h>
#include "ExpSimBase.h"
#include "../../core/AlignmentRGBD.h"

class ExpSimRGBD : public ExpSimBase
{
public:
    ExpSimRGBD();

    void
    RunNWarps(int n);

    void
    RunOneMethod(std::string s);

    void
    DumpResults();

private:

    struct Stats
    {
        std::vector<double> vdTransErr;
        std::vector<double> vdRotErr;
        std::vector<double> vdTime;
    };
    std::map<std::string,Stats> mMethodToStats;
    int nWarps;

    void
    Reset();

    void
    LoadTemplate();

    void
    GenerateWarp();

    void
    CreateReferenceImage();

    TooN::SE3<> se3;

    // Current template and reference image:
    CVD::Image<CVD::byte> imB;
    CVD::Image<unsigned short> im_depth;
    CVD::Image<CVD::byte> im_tmp;
    CVD::Image<CVD::byte> im_ref;
    Camera::Linear cam_params;
    double dTmpNoiseSigma;
    double dRefNoiseSigma;
    double dRefOutlierRatio;

    bool mbRecordAcc;
};

#endif // EXPSIMRGBD_H
