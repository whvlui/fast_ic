// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cvd/image_io.h>
#include <cvd/image_ref.h>
#include <cvd/random.h>
#include <cvd/image_interpolate.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include "../../core/AlignmentAff.h"

TooN::Matrix<3>
GenAffineWarp()
{
    TooN::Matrix<3> m3 = TooN::Ones;
    m3[0] = TooN::makeVector(0,99,49);
    m3[1] = TooN::makeVector(99,99,0);
    TooN::SVD<3> svdA(m3);

    // Perturb the 3 canonical points with Gaussian noise
    TooN::Matrix<2,3> m23 = TooN::Zeros;
    for(int r=0; r<2; ++r)
        for(int c=0; c<3; ++c)
            m23(r,c) = m3(r,c) + 3.0*CVD::rand_g();

    TooN::Matrix<3> m3Result;
    m3Result[2] = TooN::makeVector(0,0,1);
    m3Result.slice<0,0,2,3>() = m23 * svdA.get_pinv();

    return m3Result;
}

int main()
{
    std::cout << "Image alignment test with affine warps" << std::endl;

    CVD::Image<CVD::byte> im = CVD::img_load("../data/takeo.pgm");
    CVD::Image<CVD::byte> im_ref;
    im_ref.resize(CVD::ImageRef(100,100));
    im_ref.fill(0);
    CVD::ImageRef irScan;
    do
    {
        CVD::ImageRef irTarget = irScan + CVD::ImageRef(25,62);
        im_ref[irScan] = im[irTarget];
    }while(irScan.next(im_ref.size()));

    TooN::Matrix<3> m3A = GenAffineWarp();
    TooN::SVD<3> svdA(m3A);
    TooN::Matrix<3> m3AInv = svdA.get_pinv();

    CVD::Image<CVD::byte> im_live;
    im_live.resize(CVD::ImageRef(100,100));
    im_live.fill(0);

    irScan = CVD::ImageRef(0,0);
    CVD::image_interpolate<CVD::Interpolate::Bilinear,CVD::byte> interp(im_ref);
    do
    {
        TooN::Vector<3> v3 = m3AInv * TooN::makeVector(irScan.x,irScan.y,1);
        TooN::Vector<2> v2 = TooN::project(v3);

        if(v2[0] < 0 || v2[1] < 0
                || v2[0] > im_ref.size().x
                || v2[1] > im_ref.size().y)
            continue;
        im_live[irScan] = interp[v2];
    }while(irScan.next(im_ref.size()));

    AlignmentAff aff_solver(im_ref, im_live);
    aff_solver.Align("diagonal", true);

    return 0;
}
