// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ExpSimHomo.h"
#include "../../core/AlignmentHomo.h"
#include <cvd/random.h>
#include <cvd/image_io.h>
#include <cvd/image_interpolate.h>
#include <gvars3/instances.h>
#include <TooN/SVD.h>

ExpSimHomo::ExpSimHomo()
{
    LoadTemplate();

    mMethodToStats["default"] = Stats();
    mMethodToStats["scaled_identity"] = Stats();
    mMethodToStats["diagonal"] = Stats();
    mMethodToStats["off_diagonal"] = Stats();
    mMethodToStats["esm"] = Stats();
    mMethodToStats["default_reweight"] = Stats();

    dTmpNoiseSigma = GVars3::GV3::get<double>("homo.tmp_noise_sigma",0.0);
    dRefNoiseSigma = GVars3::GV3::get<double>("homo.ref_noise_sigma",0.0);
    dRefOutlierRatio = GVars3::GV3::get<double>("homo.ref_outlier_ratio",0.0);
    dWarpSigma = GVars3::GV3::get<double>("homo.warp_sigma",0.0);
}

void
ExpSimHomo::Reset()
{
    im_tmp = imB;
    im_ref.fill(0);
}

void
ExpSimHomo::LoadTemplate()
{
    std::string sRGB = GVars3::GV3::get<std::string>("homo.rgb","");

    CVD::Image<CVD::byte> im = CVD::img_load(sRGB);
    imB.resize(CVD::ImageRef(100,100));
    imB.fill(0);
    CVD::ImageRef irScan;
    do
    {
        CVD::ImageRef irTarget = irScan + CVD::ImageRef(25,62);
        imB[irScan] = im[irTarget];
    }while(irScan.next(imB.size()));
    im_tmp = imB;
}

void
ExpSimHomo::GenerateWarp()
{
    // Input points
    std::vector<TooN::Vector<3> > cam1;
    cam1.push_back(TooN::makeVector(0,0,1));
    cam1.push_back(TooN::makeVector(99,0,1));
    cam1.push_back(TooN::makeVector(0,99,1));
    cam1.push_back(TooN::makeVector(99,99,1));

    // Perturbed points
    std::vector<TooN::Vector<3> > cam2;
    for(size_t i=0; i < cam1.size(); ++i)
    {
        TooN::Vector<3> v3 = TooN::Zeros;
        v3[2] = 1.0;
        for(int j=0; j<2; ++j)
            v3[j] = cam1[i][j] + dWarpSigma*CVD::rand_g();

        cam2.push_back(v3);
    }

    // Compute action matrix A
    TooN::Matrix<9> A = TooN::Zeros;
    for(size_t pt = 0; pt < cam1.size(); ++pt)
    {
        A[pt*2+0][0] = cam1[pt][0];
        A[pt*2+0][1] = cam1[pt][1];
        A[pt*2+0][2] = 1;
        A[pt*2+0][3] = 0;
        A[pt*2+0][4] = 0;
        A[pt*2+0][5] = 0;
        A[pt*2+0][6] = -cam1[pt][0]*cam2[pt][0];
        A[pt*2+0][7] = -cam1[pt][1]*cam2[pt][0];
        A[pt*2+0][8] = -cam2[pt][0];

        A[pt*2+1][0] = 0;
        A[pt*2+1][1] = 0;
        A[pt*2+1][2] = 0;
        A[pt*2+1][3] = cam1[pt][0];
        A[pt*2+1][4] = cam1[pt][1];
        A[pt*2+1][5] = 1;
        A[pt*2+1][6] = -cam1[pt][0]*cam2[pt][1];
        A[pt*2+1][7] = -cam1[pt][1]*cam2[pt][1];
        A[pt*2+1][8] = -cam2[pt][1];
    }

    for(int i=0; i<9; ++i)
        A[8][i] = 0.0;

    // Get homography
    TooN::SVD<> svdH(A);
    TooN::Vector<9> Hvec = svdH.get_VT()[8];

    m3Warp = TooN::Zeros;
    m3Warp[0] = Hvec.slice<0,3>();
    m3Warp[1] = Hvec.slice<3,3>();
    m3Warp[2] = Hvec.slice<6,3>();

    m3Warp = m3Warp * (1.0 / m3Warp(2,2));
}

void
ExpSimHomo::CreateReferenceImage()
{
    im_ref.resize(im_tmp.size());
    im_ref.fill(0);

    TooN::SVD<3> svdm3(m3Warp);
    TooN::Matrix<3> m3WarpInv = svdm3.get_pinv();

    CVD::ImageRef irPos;
    CVD::image_interpolate<CVD::Interpolate::Bilinear,CVD::byte> interp(im_tmp);
    do
    {
        TooN::Vector<3> v3 = m3WarpInv * TooN::makeVector(irPos.x,irPos.y,1.0);
        TooN::Vector<2> v2 = TooN::project(v3);

        if(interp.in_image(v2)==false)
            continue;

        im_ref[irPos] = interp[v2];
    }while(irPos.next(im_tmp.size()));
}

void
ExpSimHomo::RunNWarps(int n)
{
    nWarps = n;
    for(int i=0; i<n; ++i)
    {
        std::cout << "Crunching image " << i << std::endl;

        Reset();
        GenerateWarp();
        CreateReferenceImage();
        AddGaussianNoise(im_tmp, dTmpNoiseSigma);
        AddGaussianNoise(im_ref, dRefNoiseSigma);
        AddOutliers(im_ref, dRefOutlierRatio);

        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            std::string sMethod = it->first;
            RunOneMethod(sMethod);
        }
    }
}

void
ExpSimHomo::RunOneMethod(std::string s)
{
    bool bReweight = true;
    std::string sMethod = s;
    if(s=="default_reweight")
        sMethod = "default";
    if(s == "default")
        bReweight = false;
    AlignmentHomo Homosolver(im_tmp, im_ref);
    Homosolver.Align(sMethod, bReweight);

    ResultHomo result = Homosolver.DumpResult();
    Stats &stats = mMethodToStats.at(s);
    if(stats.vdTime.size() == 0)
    {
        stats.vdTime.assign(result.vdTime.size(), 0.0);
        stats.vdErr.assign(result.vdTime.size(), 0.0);
    }

    for(size_t i=0; i < stats.vdTime.size(); ++i)
    {
        double dRMSerr = ComputePixelRMSE(result.mat3_vec[i]);

        stats.vdErr[i] += dRMSerr;
        stats.vdTime[i] += result.vdTime[i];
    }
}

double
ExpSimHomo::ComputePixelRMSE(const TooN::Matrix<3> m3)
{
    CVD::ImageRef irScan;
    double dErr = 0.0;
    do
    {
        const TooN::Vector<3> v3 = TooN::makeVector(irScan.x,irScan.y,1.0);
        TooN::Vector<2> v2pred = TooN::project(m3 * v3);
        TooN::Vector<2> v2true = TooN::project(m3Warp * v3);

        dErr += TooN::norm_sq(v2pred - v2true);
    }while(irScan.next(im_tmp.size()));

    int nNumPixels = im_tmp.size().x * im_tmp.size().y;
    double dRMSerr = sqrt(dErr / nNumPixels);

    return dRMSerr;
}

void
ExpSimHomo::DumpResults()
{
    for(std::map<std::string,Stats>::iterator
        it = mMethodToStats.begin();
        it != mMethodToStats.end();
        ++it)
    {
        // Compute average:
        Stats &stats = it->second;
        for(size_t i=0; i<stats.vdTime.size();++i)
        {
            stats.vdTime[i] /= nWarps;
            stats.vdErr[i] /= nWarps;
        }
        CumSum(stats.vdTime);
    }

    std::ofstream outFile;
    std::string sOut = GVars3::GV3::get<std::string>("homo.results","");
    outFile.open(sOut);

    int n = mMethodToStats.at("default").vdTime.size();
    for(int i = 0; i < n; ++i)
    {
        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            Stats& stats = it->second;
            outFile << stats.vdTime[i] << " ";
            outFile << stats.vdErr[i] << " ";
        }
        outFile << std::endl;
    }
    outFile.close();
}
