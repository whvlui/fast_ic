// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXPSIMAFF_H
#define EXPSIMAFF_H

#include "ExpSimBase.h"
#include <TooN/TooN.h>
#include <map>

class ExpSimAff : public ExpSimBase
{
public:
    ExpSimAff();

    void
    RunNWarps(int n);

    void
    RunOneMethod(std::string s);

    void
    DumpResults();

private:

    struct Stats
    {
        std::vector<double> vdErr;
        std::vector<double> vdTime;
    };
    std::map<std::string,Stats> mMethodToStats;
    int nWarps;

    void
    Reset();

    void
    GenerateWarp();

    void
    LoadTemplate();

    void
    CreateReferenceImage();

    double
    ComputePixelRMSE(const TooN::Matrix<3> m3);

    TooN::Matrix<3> m3Warp;
    CVD::Image<CVD::byte> imB;
    CVD::Image<CVD::byte> im_tmp;
    CVD::Image<CVD::byte> im_ref;
    double dTmpNoiseSigma;
    double dRefNoiseSigma;
    double dRefOutlierRatio;
    double dWarpSigma;
};

#endif // EXPSIMAFF_H
