// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ExpSimAff.h"
#include "../../core/AlignmentAff.h"
#include <TooN/SVD.h>
#include <cvd/random.h>
#include <cvd/image_io.h>
#include <cvd/image_interpolate.h>
#include <gvars3/instances.h>

ExpSimAff::ExpSimAff()
{
    LoadTemplate();

    mMethodToStats["default"] = Stats();
    mMethodToStats["scaled_identity"] = Stats();
    mMethodToStats["diagonal"] = Stats();
    mMethodToStats["off_diagonal"] = Stats();
    mMethodToStats["esm"] = Stats();
    mMethodToStats["default_reweight"] = Stats();

    dTmpNoiseSigma = GVars3::GV3::get<double>("aff.tmp_noise_sigma",0.0);
    dRefNoiseSigma = GVars3::GV3::get<double>("aff.ref_noise_sigma",0.0);
    dRefOutlierRatio = GVars3::GV3::get<double>("aff.ref_outlier_ratio",0.0);
    dWarpSigma = GVars3::GV3::get<double>("aff.warp_sigma",0.0);
    assert(dRefOutlierRatio < 1.0);
}

void
ExpSimAff::Reset()
{
    im_tmp = imB;
    im_ref.fill(0);
}

void
ExpSimAff::LoadTemplate()
{
    std::string sRGB = GVars3::GV3::get<std::string>("aff.rgb","");

    CVD::Image<CVD::byte> im = CVD::img_load(sRGB);
    imB.resize(CVD::ImageRef(100,100));
    imB.fill(0);
    CVD::ImageRef irScan;
    do
    {
        CVD::ImageRef irTarget = irScan + CVD::ImageRef(25,62);
        imB[irScan] = im[irTarget];
    }while(irScan.next(imB.size()));
    im_tmp = imB;
}

void
ExpSimAff::GenerateWarp()
{
    TooN::Matrix<3> m3 = TooN::Ones;
    m3[0] = TooN::makeVector(0,99,49);
    m3[1] = TooN::makeVector(99,99,0);
    TooN::SVD<3> svdA(m3);

    // Perturb the 3 canonical points with Gaussian noise
    TooN::Matrix<2,3> m23 = TooN::Zeros;
    for(int r=0; r<2; ++r)
        for(int c=0; c<3; ++c)
            m23(r,c) = m3(r,c) + dWarpSigma*CVD::rand_g();

    m3Warp = TooN::Zeros;
    m3Warp[2] = TooN::makeVector(0,0,1);
    m3Warp.slice<0,0,2,3>() = m23 * svdA.get_pinv();
}

void
ExpSimAff::CreateReferenceImage()
{
    im_ref.resize(im_tmp.size());
    im_ref.fill(0);

    TooN::SVD<3> svdm3(m3Warp);
    TooN::Matrix<3> m3WarpInv = svdm3.get_pinv();

    CVD::ImageRef irPos;
    CVD::image_interpolate<CVD::Interpolate::Bilinear,CVD::byte> interp(im_tmp);
    do
    {
        TooN::Vector<3> v3 = m3WarpInv * TooN::makeVector(irPos.x,irPos.y,1.0);
        TooN::Vector<2> v2 = TooN::project(v3);

        if(interp.in_image(v2)==false)
            continue;

        im_ref[irPos] = interp[v2];
    }while(irPos.next(im_tmp.size()));
}

void
ExpSimAff::RunNWarps(int n)
{
    nWarps = n;
    for(int i=0; i<n; ++i)
    {
        std::cout << "Crunching image " << i << std::endl;

        Reset();
        GenerateWarp();
        CreateReferenceImage();
        AddGaussianNoise(im_tmp, dTmpNoiseSigma);
        AddGaussianNoise(im_ref, dRefNoiseSigma);
        AddOutliers(im_ref, dRefOutlierRatio);

        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            std::string sMethod = it->first;
            RunOneMethod(sMethod);
        }
    }
}

void
ExpSimAff::RunOneMethod(std::string s)
{
    bool bReweight = true;
    std::string sMethod=s;
    if(s=="default_reweight")
        sMethod = "default";
    if(s == "default")
        bReweight = false;
    AlignmentAff Affsolver(im_tmp, im_ref);
    Affsolver.Align(sMethod, bReweight);

    ResultAff result = Affsolver.DumpResult();
    Stats &stats = mMethodToStats.at(s);
    if(stats.vdTime.size() == 0)
    {
        stats.vdTime.assign(result.vdTime.size(), 0.0);
        stats.vdErr.assign(result.vdTime.size(), 0.0);
    }

    for(size_t i=0; i < stats.vdTime.size(); ++i)
    {
        double dRMSerr = ComputePixelRMSE(result.mat3_vec[i]);

        stats.vdErr[i] += dRMSerr;
        stats.vdTime[i] += result.vdTime[i];
    }
}

double
ExpSimAff::ComputePixelRMSE(const TooN::Matrix<3> m3)
{
    CVD::ImageRef irScan;
    double dErr = 0.0;
    do
    {
        const TooN::Vector<3> v3 = TooN::makeVector(irScan.x,irScan.y,1.0);
        TooN::Vector<2> v2pred = TooN::project(m3 * v3);
        TooN::Vector<2> v2true = TooN::project(m3Warp * v3);

        dErr += TooN::norm_sq(v2pred - v2true);
    }while(irScan.next(im_tmp.size()));

    int nNumPixels = im_tmp.size().x * im_tmp.size().y;
    double dRMSerr = sqrt(dErr / nNumPixels);

    return dRMSerr;
}

void
ExpSimAff::DumpResults()
{
    for(std::map<std::string,Stats>::iterator
        it = mMethodToStats.begin();
        it != mMethodToStats.end();
        ++it)
    {
        // Compute average:
        Stats &stats = it->second;
        for(size_t i=0; i<stats.vdTime.size();++i)
        {
            stats.vdTime[i] /= nWarps;
            stats.vdErr[i] /= nWarps;
        }
        CumSum(stats.vdTime);
    }

    std::ofstream outFile;
    std::string sOut = GVars3::GV3::get<std::string>("aff.results","");
    outFile.open(sOut);

    int n = mMethodToStats.at("default").vdTime.size();
    for(int i = 0; i < n; ++i)
    {
        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            Stats& stats = it->second;
            outFile << stats.vdTime[i] << " ";
            outFile << stats.vdErr[i] << " ";
        }
        outFile << std::endl;
    }
    outFile.close();
}
