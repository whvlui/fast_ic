// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXPSIMBASE_H
#define EXPSIMBASE_H

#include <cvd/image.h>
#include <cvd/byte.h>
#include <vector>

class ExpSimBase
{
public:
    ExpSimBase();

    static
    ExpSimBase* Create(std::string s);

    virtual
    void RunNWarps(int n)=0;

    virtual
    void RunOneMethod(std::string s)=0;

    virtual
    void DumpResults()=0;

protected:

    void
    AddGaussianNoise(CVD::Image<CVD::byte> &im, double dSigma);

    void
    AddOutliers(CVD::Image<CVD::byte> &im, double dRatio);

    void
    CumSum(std::vector<double> &vec);

    virtual
    void Reset()=0;

    virtual
    void LoadTemplate()=0;

    virtual
    void GenerateWarp()=0;

    virtual
    void CreateReferenceImage()=0;
};

#endif // EXPSIMBASE_H
