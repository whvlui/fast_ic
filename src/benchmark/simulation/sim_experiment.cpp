// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>
#include <cvd/image_io.h>
#include <gvars3/instances.h>
#include "ExpSimRGBD.h"
#include "ExpSimAff.h"
#include "ExpSimHomo.h"

using namespace std;
using namespace CVD;

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        std::cerr << "[sim_experiment]: Need argument to select motion model!" << std::endl;
        std::cerr << "Usage: ./sim_experiment motion_model, where motion model can be" << std::endl;
        std::cerr << "se3, affine, or homography" << std::endl;
        return -1;
    }

    std::string s(argv[1]);
    if( (s != "se3") && (s != "affine") && (s != "homography"))
    {
        std::cerr << "[sim_experiment]: The option " << s << " does not exist!" << std::endl;
        std::cerr << "Choose one of se3, affine, or homography" << std::endl;
        return -1;
    }

    GVars3::GUI.LoadFile("../cfg/sim_params.cfg");

    srand(time(NULL));
    srand48(time(NULL));

    ExpSimBase* experiment;
    experiment = ExpSimBase::Create(s);
    experiment->RunNWarps(10);
    experiment->DumpResults();

    return 0;
}
