// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "Experiment.h"
#include <iostream>
#include <gvars3/instances.h>

using namespace std;

int main()
{
    cout << "Real data experiment using TUM RGB-D dataset!" << endl;

    GVars3::GUI.LoadFile("../cfg/real_params.cfg");

    int nWarps = GVars3::GV3::get<int>("exp.num_warps",100);
    Experiment exp;
    exp.RunNImages(nWarps);

    return 0;
}
