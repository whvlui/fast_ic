// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "Experiment.h"
#include <fstream>
#include <gvars3/instances.h>
#include <gvars3/GStringUtil.h>
#include <cvd/image_io.h>
#include <cvd/image.h>
#include <cvd/byte.h>

Experiment::Experiment()
{
    sDataFolder = GVars3::GV3::get<std::string>("exp.data_folder","");
    LoadDataset();

    mMethodToStats["default"] = Stats();
    mMethodToStats["scaled_identity"] = Stats();
    mMethodToStats["diagonal"] = Stats();
    mMethodToStats["off_diagonal"] = Stats();
    mMethodToStats["esm"] = Stats();
    mMethodToStats["default_reweight"] = Stats();
}

void
Experiment::LoadDataset()
{
    // (1) Load ground truth
    std::ifstream ground_truth;
    std::string sGT_association = GVars3::GV3::get<std::string>("exp.gt_association","");
    std::string s_gt = sDataFolder + sGT_association;
    ground_truth.open(s_gt);

    while(ground_truth.good())
    {
        std::string sLine;
        std::getline(ground_truth, sLine);

        std::vector<std::string> s_vec = GVars3::ChopAndUnquoteString(sLine);
        if(s_vec.size() != 8)
            continue;

        TooN::Vector<3> v3t = TooN::makeVector(std::stod(s_vec[1]),std::stod(s_vec[2]),std::stod(s_vec[3]));
        TooN::Vector<4> v4q_xyzw = TooN::makeVector(std::stod(s_vec[4]),std::stod(s_vec[5]),std::stod(s_vec[6]),std::stod(s_vec[7]));

        TooN::SO3<> so3 = quaternionToSO3(v4q_xyzw);
        TooN::SE3<> se3(so3,v3t);

        mID_gt_abs_poses[s_vec[0]] = se3;
    }

    // (2) Load association between RGB image and depth image
    std::ifstream rgb_to_d;
    std::string sRGBD_association = GVars3::GV3::get<std::string>("exp.rgbd_association","");
    std::string sRGB_to_D = sDataFolder + sRGBD_association;
    rgb_to_d.open(sRGB_to_D);

    while(rgb_to_d.good())
    {
        std::string sLine;
        std::getline(rgb_to_d, sLine);

        std::vector<std::string> s_vec = GVars3::ChopAndUnquoteString(sLine);
        if(s_vec.size() != 2)
            continue;

        mRgbID_depthID[s_vec[0]] = s_vec[1];
    }
}

TooN::SO3<> Experiment::quaternionToSO3(TooN::Vector<4> v4q_xyzw)
{
    double x = v4q_xyzw[0];
    double y = v4q_xyzw[1];
    double z = v4q_xyzw[2];
    double w = v4q_xyzw[3];

    TooN::Matrix<3> m3 = TooN::Zeros;

    m3[0][0] = 1.0 - 2.0 * (y*y + z*z);
    m3[1][1] = 1.0 - 2.0 * (x*x + z*z);
    m3[2][2] = 1.0 - 2.0 * (x*x + y*y);
    m3[0][1] = 2 * (x*y - w*z);
    m3[0][2] = 2 * (x*z + w*y);
    m3[1][0] = 2 * (x*y + w*z);
    m3[1][2] = 2 * (y*z - w*x);
    m3[2][0] = 2 * (x*z - w*y);
    m3[2][1] = 2 * (y*z + w*x);

    TooN::SO3<> result(m3);
    return result;
}

void Experiment::RunNImages(int n)
{
    const int OFFSET = 1;
    nWarps = 0;
    std::map<std::string,std::string>::iterator it = mRgbID_depthID.begin();
    std::advance(it, OFFSET);
    for(; it != mRgbID_depthID.end(); ++it)
    {
        std::map<std::string,std::string>::iterator tmp_it = it;
        std::advance(tmp_it, -OFFSET);

        if(mID_gt_abs_poses.count(tmp_it->first) == 0 ||
                mID_gt_abs_poses.count(it->first) == 0)
            continue;

        std::cout << "Crunching image " << it->first << std::endl;

        std::stringstream ss_im_tmp, ss_depth_tmp, ss_im_ref;
        std::string sRGB = GVars3::GV3::get<std::string>("exp.rgb","");
        std::string sDepth = GVars3::GV3::get<std::string>("exp.depth","");

        ss_im_tmp << sDataFolder << sRGB  << tmp_it->first << ".png";
        ss_depth_tmp << sDataFolder << sDepth << tmp_it->second << ".png";
        ss_im_ref << sDataFolder << sRGB << it->first << ".png";

        im_tmpB = CVD::img_load(ss_im_tmp.str());
        im_depth_tmp = CVD::img_load(ss_depth_tmp.str());
        im_ref = CVD::img_load(ss_im_ref.str());

        TooN::SE3<> se3_gt_tmp = mID_gt_abs_poses.at(tmp_it->first);
        TooN::SE3<> se3_gt_ref = mID_gt_abs_poses.at(it->first);
        se3_true = se3_gt_tmp.inverse() * se3_gt_ref;

        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            bool bReweight=true;
            std::string s = it->first;
            if(s == "default")
                bReweight = false;
            ComputeAlignment(it->first, bReweight);
        }

        ++nWarps;
        if(nWarps >= n-1)
            break;
    }

    std::cout << "Number of warps tested: " << nWarps << std::endl;
    DumpResults();
}

void
Experiment::DumpResults()
{
    for(std::map<std::string,Stats>::iterator
        it = mMethodToStats.begin();
        it != mMethodToStats.end();
        ++it)
    {
        // Compute average:
        Stats &stats = it->second;
        for(size_t i=0; i<stats.vdTime.size(); ++i)
        {
            stats.vdTime[i] /= nWarps;
            stats.vdTransErr[i] /= nWarps;
            stats.vdRotErr[i] /= nWarps;
        }

        double dCumSum=0.0;
        for(size_t i=0; i<stats.vdTime.size(); ++i)
        {
            dCumSum += stats.vdTime[i];
            stats.vdTime[i] = dCumSum;
        }
    }

    // Dump results
    std::ofstream outFile;
    std::string sOut = GVars3::GV3::get<std::string>("exp.results","");
    outFile.open(sOut);

    int n = mMethodToStats.at("default").vdTime.size();
    std::cout << "number of rows: " << n << std::endl;
    for(int i = 0; i < n; ++i)
    {
        for(std::map<std::string,Stats>::iterator
            it = mMethodToStats.begin();
            it != mMethodToStats.end();
            ++it)
        {
            Stats& stats = it->second;
            outFile << stats.vdTime[i] << " ";
            outFile << stats.vdTransErr[i] << " ";
            outFile << stats.vdRotErr[i] << " ";
        }
        outFile << std::endl;
    }
    outFile.close();
}

void Experiment::ComputeAlignment(std::string s, bool bReweight)
{
    std::string sMethod = s;
    if(s == "default_reweight")
        sMethod = "default";
    AlignmentRGBD ia(im_tmpB, im_depth_tmp, im_ref);
    ia.Align(sMethod, bReweight);

    Result r = ia.DumpResult();
    Stats &stats = mMethodToStats.at(s);

    // Accumulate results
    if(stats.vdTime.size() == 0)
    {
        stats.vdTime.assign(r.vdTime.size(), 0.0);
        stats.vdTransErr.assign(r.vdTime.size(), 0.0);
        stats.vdRotErr.assign(r.vdTime.size(), 0.0);
    }

    for(size_t i = 0; i < stats.vdTime.size(); ++i)
    {
        TooN::SE3<> se3_err = se3_true * r.se3_vec[i];
        double dTransErr = TooN::norm(se3_err.get_translation());
        double dRotErr = TooN::norm(se3_err.get_rotation().ln());

        stats.vdTransErr[i] += dTransErr;
        stats.vdRotErr[i] += dRotErr;
        stats.vdTime[i] += r.vdTime[i];
    }
}
