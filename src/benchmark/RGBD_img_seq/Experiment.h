// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <vector>
#include <map>
#include <TooN/se3.h>
#include <cvd/image.h>
#include <cvd/byte.h>
#include "../../core/AlignmentRGBD.h"

class Experiment
{
public:
    Experiment();

    void
    RunNImages(int n);

    void
    DumpResults();

    void
    LoadDataset();
private:

    struct Stats
    {
        std::vector<double> vdTransErr;
        std::vector<double> vdRotErr;
        std::vector<double> vdTime;
    };
    std::map<std::string,Stats> mMethodToStats;
    int nWarps;

    void
    ComputeAlignment(std::string s, bool bReweight);

    TooN::SO3<>
    quaternionToSO3(TooN::Vector<4> v4q_xyzw);

    std::vector<TooN::SE3<> > gt_abs_poses;
    std::map<std::string, TooN::SE3<> > mID_gt_abs_poses;
    std::map<std::string, std::string> mRgbID_depthID;
    TooN::SE3<> se3_true;

    // Current template and reference image:
    CVD::Image<CVD::byte> im_tmpB;
    CVD::Image<unsigned short> im_depth_tmp;
    CVD::Image<CVD::byte> im_ref;
    std::string sDataFolder;
};

#endif // EXPERIMENT_H
