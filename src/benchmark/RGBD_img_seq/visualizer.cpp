// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <TooN/se3.h>
#include <gvars3/instances.h>
#include <gvars3/GStringUtil.h>
#include "../../../thirdparty/GLWindow2.h"

void DrawCamera()
{
    glBegin(GL_LINES);
    glColor3f(1,0,0);
    glVertex3f(0,0,0);
    glVertex3f(1,0,0);
    glColor3f(0,1,0);
    glVertex3f(0,0,0);
    glVertex3f(0,1,0);
    glColor3f(0,0,1);
    glVertex3f(0,0,0);
    glVertex3f(0,0,1);
    glEnd();
}

void DrawSmallCamera()
{
    glBegin(GL_LINES);
    glColor3f(1,0,0);
    glVertex3f(0,0,0);
    glVertex3f(0.01,0,0);
    glColor3f(0,1,0);
    glVertex3f(0,0,0);
    glVertex3f(0,0.01,0);
    glColor3f(0,0,1);
    glVertex3f(0,0,0);
    glVertex3f(0,0,0.01);
    glEnd();
}

int main()
{
    // (1) Load results
    std::ifstream results;
    GVars3::GUI.LoadFile("../cfg/vis_odom.cfg");
    std::string sResults = GVars3::GV3::get<std::string>("vis_odom.results","");
    results.open(sResults);

    std::vector<TooN::SE3<> > relative_poses;
    while(results.good())
    {
        std::string sLine;
        std::getline(results, sLine);
        std::vector<std::string> vs = GVars3::ChopAndUnquoteString(sLine);

        if(vs.size() != 6)
            continue;

        TooN::Vector<6> v6 = TooN::makeVector(stod(vs[0]),stod(vs[1]),stod(vs[2]),stod(vs[3]),stod(vs[4]),stod(vs[5]));
        relative_poses.push_back(TooN::SE3<>::exp(v6));
    }

    // (2) Visualize results
    GLWindow2 glw(CVD::ImageRef(640,480),"Visual odometry results");
    TooN::SE3<> se3ViewerFromOrigin;
    while(1)
    {
        glClearColor(1,1,1,1);
        glClear(GL_COLOR_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(70,1,0.1,100);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        CVD::glMultMatrix(TooN::SO3<>::exp(TooN::makeVector(M_PI,0,0)));
        CVD::glMultMatrix(TooN::SE3<>::exp(TooN::makeVector(0,0,-5,0,0,0)));

        std::pair<TooN::Vector<6>,TooN::Vector<6> > pv6 = glw.GetMousePoseUpdate();
        TooN::SE3<> se3CamFromMC;
        se3CamFromMC.get_translation() = se3ViewerFromOrigin * TooN::makeVector(0,0,0);

        se3ViewerFromOrigin = TooN::SE3<>::exp(pv6.first) * se3CamFromMC *
                TooN::SE3<>::exp(pv6.second) * se3CamFromMC.inverse() * se3ViewerFromOrigin;
        CVD::glMultMatrix(se3ViewerFromOrigin);

        DrawCamera();
        for(size_t i = 0; i < relative_poses.size(); ++i)
        {
            CVD::glMultMatrix(relative_poses[i].inverse());
            DrawSmallCamera();
        }
        glw.swap_buffers();
        glw.HandlePendingEvents();
    }
    return 0;
}
