// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>
#include <fstream>
#include <cvd/image_io.h>
#include <TooN/se3.h>
#include <gvars3/instances.h>
#include <gvars3/GStringUtil.h>
#include "../../../thirdparty/GLWindow2.h"
#include "../../core/AlignmentRGBD.h"

int main()
{
    GVars3::GUI.LoadFile("../cfg/vis_odom.cfg");
    std::string sDataFolder = GVars3::GV3::get<std::string>("vis_odom.data_folder","");
    std::string sRGBD_association = GVars3::GV3::get<std::string>("vis_odom.rgbd_association","");
    std::string sRGB = GVars3::GV3::get<std::string>("vis_odom.rgb","");
    std::string sDepth = GVars3::GV3::get<std::string>("vis_odom.depth","");
    const int NUM_IMAGES = GVars3::GV3::get<int>("vis_odom.num_images",700);

    // Load association between RGB image
    // and depth image
    std::ifstream rgb_to_d;
    std::string sRGBtoD = sDataFolder + sRGBD_association;
    rgb_to_d.open(sRGBtoD);

    std::map<std::string,std::string> mRgbID_depthID;
    while(rgb_to_d.good())
    {
        std::string sLine;
        std::getline(rgb_to_d,sLine);

        std::vector<std::string> s_vec = GVars3::ChopAndUnquoteString(sLine);
        if(s_vec.size() != 2)
            continue;

        mRgbID_depthID[s_vec[0]] = s_vec[1];
    }
    rgb_to_d.close();

    std::ofstream outFile;
    std::string sResults = GVars3::GV3::get<std::string>("vis_odom.results","");
    outFile.open(sResults);

    int nImages=0;
    std::map<std::string,std::string>::iterator it = mRgbID_depthID.begin();
    std::advance(it,1);
    for(; it != mRgbID_depthID.end(); ++it)
    {
        std::map<std::string,std::string>::iterator tmp_it = it;
        std::advance(tmp_it, -1);

        std::cout << "Crunching image " << tmp_it->first << ".png ..." << std::endl;

        std::stringstream ss_im_tmp, ss_depth_tmp, ss_im_ref;
        ss_im_tmp << sDataFolder << sRGB << tmp_it->first << ".png";
        ss_depth_tmp << sDataFolder << sDepth << tmp_it->second << ".png";
        ss_im_ref << sDataFolder << sRGB << it->first << ".png";

        CVD::Image<CVD::byte> im_tmpB = CVD::img_load(ss_im_tmp.str());
        CVD::Image<unsigned short> im_depth_tmp = CVD::img_load(ss_depth_tmp.str());
        CVD::Image<CVD::byte> im_ref = CVD::img_load(ss_im_ref.str());

        AlignmentRGBD se3solver(im_tmpB,im_depth_tmp,im_ref);
        se3solver.Align("diagonal", true);

        Result result = se3solver.DumpResult();
        TooN::SE3<> se3 = result.se3_vec.back();
        outFile << se3.ln() << std::endl;

        ++nImages;
        if(nImages > NUM_IMAGES)
            break;
    }

    return 0;
}
