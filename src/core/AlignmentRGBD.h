// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ALIGNMENTRGBD_H
#define ALIGNMENTRGBD_H

#include <cvd/image.h>
#include <cvd/byte.h>
#include <cvd/camera.h>
#include <TooN/TooN.h>
#include <TooN/se3.h>
#include "QR_fast.h"

struct Result
{
    std::vector<TooN::SE3<> > se3_vec;
    std::vector<double> vdRes;  // residual photometric error
    std::vector<double> vdObj;  //objective score
    std::vector<double> vdErr;  // translational error
    std::vector<double> vdErr_rot;  // rotational error
    std::vector<double> vdTime;
    std::vector<double> vdPcRms;
};

class AlignmentRGBD
{
public:
    enum { DIM = 6 };
protected:

    static const int MIN_LEVEL=0;
    static const int MAX_LEVEL=2;
    static const int NUM_LEVELS=MAX_LEVEL+1;

    struct Data
    {
        int num_data_points;
        CVD::Image<CVD::byte> im_ref;
        CVD::Image<CVD::byte> im_live;
        CVD::Image<CVD::byte> pred_im;
        CVD::Image<float[2]> gradient_im;
        CVD::Image<unsigned short> depth_im;
        Camera::Linear lin_cam;

        struct PixelData
        {
            PixelData();

            PixelData(TooN::Vector<3> v3,
                      TooN::Vector<2> v2,
                      CVD::ImageRef irLoc,
                      CVD::byte intensity)
                : v3XYZ(v3), v2Gradient(v2), ir(irLoc), ucIntensity(intensity)
            {
                bNoData = false;
            }

            bool bNoData;
            CVD::ImageRef ir;
            CVD::byte ucIntensity;

            TooN::Vector<2> v2Gradient;
            TooN::Vector<3> v3XYZ;

            // Intermediates:
            TooN::Matrix<2> m2CamDerivs;
            TooN::Vector<3> v3Cam;
            TooN::Vector<2> v2Image;
            CVD::byte ucPredIntensity;
            double error;

            // Row of Jacobian and Q factor
            TooN::Matrix<2,DIM> m26;
            TooN::Vector<DIM> v6Jacobian;
            TooN::Vector<DIM> v6Q;
        };
        TooN::Matrix<DIM> m6R;
        TooN::Matrix<DIM> m6HInv;
        std::vector<PixelData> vPixData;
    };
    std::vector<Data> my_data;
    TooN::SE3<> se3;

    void
    BuildPyramid(CVD::Image<CVD::byte> &refImB,
                 CVD::Image<unsigned short> &depthIm,
                 CVD::Image<CVD::byte> &liveImB);

    TooN::Vector<3>
    DepthToXYZ(unsigned short d, CVD::ImageRef ir, const TooN::Vector<4> &v4calib);

    bool
    DepthInvalid(TooN::Vector<3> &v3);

    double factor; // Scale factor between true depth and kinect depth map value
    Result result;

public:

    AlignmentRGBD(CVD::Image<CVD::byte> &refImB,
                   CVD::Image<unsigned short> &depthIm,
                   CVD::Image<CVD::byte> &liveImB);

    ~AlignmentRGBD(){}

    void
    Align(std::string s, bool bReweight);

    inline Result
    DumpResult(){return result;}

    void
    InitImage(int nLvl);

    bool
    CreateDataPoint(CVD::ImageRef irPos, int nLvl);

    TooN::Vector<DIM>
    ComputeJtarget(int nIdx, TooN::Vector<2> v2grad, int nLvl);

    CVD::byte
    CreatePrediction(int nIdx, const TooN::Vector<3> &v3, int nLvl);

    void
    ComputeGradPredIm(CVD::Image<float[2]> &grad_pred_im, int nLvl);

    double
    ComputeError(int nIdx, int nLvl);

    TooN::Vector<3>
    Project(const TooN::Vector<3> &v3xyz);

    void
    UpdateWarp(const TooN::Vector<DIM> &v6up);

    // Getters and setters
    inline const CVD::ImageRef
    GetImageSize(int nLvl) const
    {
        return my_data[nLvl].im_live.size();
    }

    inline const int
    GetNumDataPoints(int nLvl) const
    {
        return my_data[nLvl].num_data_points;
    }

    inline const bool
    bHasNoData(int nIdx, int nLvl) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.bNoData;
    }

    inline const CVD::ImageRef
    GetImPos(int nIdx, int nLvl) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.ir;
    }

    inline const TooN::Vector<DIM>
    GetJsource(int nIdx, int nLvl) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.v6Jacobian;
    }

    inline void
    SetQrow(int nIdx, const TooN::Vector<DIM> &v6, int nLvl)
    {
        std::vector<Data::PixelData>::iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        Data::PixelData &pd = *it;
        pd.v6Q = v6;
    }

    inline const TooN::Vector<DIM>
    GetQrow(int nIdx, int nLvl) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.v6Q;
    }

    inline const TooN::Vector<3>
    GetVec3Meas(int nIdx, int nLvl) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.v3XYZ;
    }

    inline const double
    GetError(int nIdx, int nLvl) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data[nLvl].vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.error;
    }

    inline void
    SetHInv(TooN::Matrix<DIM> &mDimHInv, int nLvl)
    {
        my_data[nLvl].m6HInv = mDimHInv;
    }

    inline const TooN::Matrix<DIM>
    GetHInv(int nLvl) const
    {
        return my_data[nLvl].m6HInv;
    }

    inline void
    SetR(TooN::Matrix<DIM> &mDimR, int nLvl)
    {
        my_data[nLvl].m6R = mDimR;
    }

    inline const TooN::Matrix<DIM>
    GetR(int nLvl) const
    {
        return my_data[nLvl].m6R;
    }
};

#endif // ALIGNMENTRGBD_H
