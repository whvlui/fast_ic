// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "AlignmentRGBD.h"
#include "timer.h"
#include "AlignAlg.h"
#include <TooN/se3.h>
#include <TooN/Cholesky.h>
#include <cvd/vision.h>
#include <cvd/image_interpolate.h>
#include <fstream>

using namespace std;
using namespace TooN;
using namespace CVD;

AlignmentRGBD::AlignmentRGBD(CVD::Image<CVD::byte> &refImB,
                               CVD::Image<unsigned short> &depthIm,
                               CVD::Image<CVD::byte> &liveImB)
{
    factor = 5000.0;
    BuildPyramid(refImB, depthIm, liveImB);
}

TooN::Vector<3>
AlignmentRGBD::DepthToXYZ(unsigned short d, CVD::ImageRef ir, const TooN::Vector<4> &v4calib)
{
    TooN::Vector<4> v4 = v4calib;

    double Z = (double) d / factor;
    double X = (ir.x - v4[2]) * Z / v4[0];
    double Y = (ir.y - v4[3]) * Z / v4[1];
    TooN::Vector<3> v3 = TooN::makeVector(X,Y,Z);

    return v3;
}

bool
AlignmentRGBD::DepthInvalid(TooN::Vector<3> &v3)
{
    return (v3[0]==0 && v3[1]==0 && v3[2]==0);
}

TooN::Vector<3>
AlignmentRGBD::Project(const TooN::Vector<3> &v3xyz)
{
    return se3 * v3xyz;
}

void
AlignmentRGBD::InitImage(int nLvl)
{
    my_data[nLvl].pred_im.resize(my_data[nLvl].im_live.size());
    my_data[nLvl].pred_im.fill(0);
    my_data[nLvl].num_data_points = 0;
}

bool
AlignmentRGBD::CreateDataPoint(CVD::ImageRef irPos, int nLvl)
{
    TooN::Vector<3> v3_xyz = DepthToXYZ(my_data[nLvl].depth_im[irPos],
                                        irPos,
                                        my_data[nLvl].lin_cam.get_parameters());

    if(DepthInvalid(v3_xyz))
        return false;

    CVD::byte i = my_data[nLvl].im_ref[irPos];
    TooN::Vector<2> v2grad = TooN::Zeros;
    v2grad[0] = my_data[nLvl].gradient_im[irPos][0];
    v2grad[1] = my_data[nLvl].gradient_im[irPos][1];

    if(norm(v2grad) < 0.1)
        return false;
    my_data[nLvl].vPixData.push_back(Data::PixelData(v3_xyz,v2grad,irPos,i));
    my_data[nLvl].num_data_points++;

    Data::PixelData &pd = my_data[nLvl].vPixData.back();

    // Pre-compute jacobian
    pd.m2CamDerivs = TooN::Zeros;
    pd.m2CamDerivs[0][0] = my_data[nLvl].lin_cam.get_parameters()[0];
    pd.m2CamDerivs[1][1] = my_data[nLvl].lin_cam.get_parameters()[1];
    double dOneOverCameraZ = 1.0 / pd.v3XYZ[2];
    for(int m = 0; m < 6; ++m)
    {
        const TooN::Vector<4> v4Motion = TooN::SE3<>::generator_field(m, TooN::unproject(pd.v3XYZ));
        TooN::Vector<2> v2CamFrameMotion;
        v2CamFrameMotion[0] = (v4Motion[0] - pd.v3XYZ[0] * v4Motion[2] * dOneOverCameraZ) * dOneOverCameraZ;
        v2CamFrameMotion[1] = (v4Motion[1] - pd.v3XYZ[1] * v4Motion[2] * dOneOverCameraZ) * dOneOverCameraZ;
        pd.m26.T()[m] = pd.m2CamDerivs * v2CamFrameMotion;
        pd.v6Jacobian[m] = pd.v2Gradient[0] * pd.m26[0][m] + pd.v2Gradient[1] * pd.m26[1][m];
    };

    return true;
}

TooN::Vector<AlignmentRGBD::DIM>
AlignmentRGBD::ComputeJtarget(int nIdx, TooN::Vector<2> v2grad, int nLvl)
{
    std::vector<Data::PixelData>::iterator it = my_data[nLvl].vPixData.begin();
    std::advance(it,nIdx);
    Data::PixelData &pd = *it;

    TooN::Vector<DIM> vDimJ = TooN::Zeros;
    if(pd.bNoData)
        return vDimJ;

    double dOneOverCameraZ = 1.0 / pd.v3Cam[2];
    for(int m = 0; m < 6; ++m)
    {
        const Vector<4> v4Motion = SE3<>::generator_field(m, unproject(pd.v3Cam));
        Vector<2> v2CamFrameMotion;
        v2CamFrameMotion[0] = (v4Motion[0] - pd.v3Cam[0] * v4Motion[2] * dOneOverCameraZ) * dOneOverCameraZ;
        v2CamFrameMotion[1] = (v4Motion[1] - pd.v3Cam[1] * v4Motion[2] * dOneOverCameraZ) * dOneOverCameraZ;
        pd.m26.T()[m] = pd.m2CamDerivs * v2CamFrameMotion;
        vDimJ[m] = v2grad[0] * pd.m26[0][m] + v2grad[1] * pd.m26[1][m];
    }

    return vDimJ;
}

CVD::byte
AlignmentRGBD::CreatePrediction(int nIdx, const TooN::Vector<3> &v3, int nLvl)
{
    std::vector<Data::PixelData>::iterator it = my_data[nLvl].vPixData.begin();
    std::advance(it,nIdx);
    Data::PixelData &pd = *it;

    pd.v3Cam = v3;
    pd.v2Image = my_data[nLvl].lin_cam.project(project(pd.v3Cam));

    CVD::image_interpolate<CVD::Interpolate::Bilinear, CVD::byte>
            interp(my_data[nLvl].im_live);
    if(interp.in_image(pd.v2Image))
        pd.ucPredIntensity = interp[pd.v2Image];
    else
    {
        pd.ucPredIntensity = 0;
        pd.bNoData = true;
    }

    my_data[nLvl].pred_im[pd.ir] = pd.ucPredIntensity;

    return pd.ucPredIntensity;
}

void
AlignmentRGBD::ComputeGradPredIm(CVD::Image<float[2]> &grad_pred_im, int nLvl)
{
    grad_pred_im.resize(my_data[nLvl].pred_im.size());
    CVD::gradient(my_data[nLvl].pred_im,
                  grad_pred_im);
}

double
AlignmentRGBD::ComputeError(int nIdx, int nLvl)
{
    std::vector<Data::PixelData>::iterator it = my_data[nLvl].vPixData.begin();
    std::advance(it,nIdx);
    Data::PixelData &pd = *it;

    if(pd.bNoData)
        pd.error = 0;
    else
        pd.error = ((double)(pd.ucPredIntensity - pd.ucIntensity))/255.0;

    return pd.error;
}

void
AlignmentRGBD::UpdateWarp(const TooN::Vector<AlignmentRGBD::DIM> &v6up)
{
    SE3<> se3up = SE3<>::exp(-v6up);
    se3 = se3 * se3up;
}

void
AlignmentRGBD::BuildPyramid(CVD::Image<CVD::byte> &refImB,
                             CVD::Image<unsigned short> &depthIm,
                             CVD::Image<CVD::byte> &liveImB)
{
    my_data.resize(NUM_LEVELS);
    std::fstream inFile;
    inFile.open("../data/kinect_params.txt");
    my_data[0].lin_cam.load(inFile);
    inFile.close();

    my_data[0].im_ref = refImB;
    my_data[0].im_live = liveImB;
    my_data[0].depth_im = depthIm;
    my_data[0].gradient_im.resize(refImB.size());
    CVD::gradient(my_data[0].im_ref, my_data[0].gradient_im);

    for(int i = 1; i < NUM_LEVELS; ++i)
    {
        CVD::ImageRef irSize = my_data[i-1].im_ref.size();
        irSize /= 2;
        my_data[i].im_ref.resize(irSize);
        my_data[i].im_live.resize(irSize);
        my_data[i].depth_im.resize(irSize);
        my_data[i].gradient_im.resize(irSize);
        halfSample(my_data[i-1].im_ref, my_data[i].im_ref);
        halfSample(my_data[i-1].im_live, my_data[i].im_live);
        halfSample(my_data[i-1].depth_im, my_data[i].depth_im);
        my_data[i].lin_cam.get_parameters() = 0.5 * my_data[i-1].lin_cam.get_parameters();
        CVD::gradient(my_data[i].im_ref, my_data[i].gradient_im);
    }
}

void
AlignmentRGBD::Align(std::string s, bool bReweight)
{

    // Initialize results
    result.vdTime.push_back(0.0);
    result.se3_vec.push_back(se3);

    AlignAlg<AlignmentRGBD> *align_alg;
    align_alg = AlignAlg<AlignmentRGBD>::Create(s);

    for(int lvl = MAX_LEVEL; lvl >= MIN_LEVEL; --lvl)
    {
        int max_iter = 20;
        if(lvl == 2)
            max_iter = 20;
        else if(lvl == 1)
            max_iter = 10;
        else if(lvl == 0)
            max_iter = 10;

        align_alg->Init(*this,lvl);

        for(int k = 0; k < max_iter; ++k)
        {
            Timer timer;
            timer.start_timer();
            my_data[lvl].pred_im.fill(0.0);
            align_alg->Solve(*this, bReweight, lvl);

            // Log results
            timer.end_timer();
            result.vdTime.push_back(timer.get_time_mS());
            result.se3_vec.push_back(se3);
        }
    }

    delete align_alg;
}
