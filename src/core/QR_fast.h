// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef QR_FAST_H
#define QR_FAST_H

#include <TooN/TooN.h>

template<int Rows=TooN::Dynamic, int Cols=TooN::Dynamic, typename Precision=double>
class QR_fast
{
public:
    // The input matrix will be consumed by this object
    // because I will most likely deal with tall, thin
    // matrices with lots of rows. Hence I don't want to
    // incur the copying cost.
    template<int R, int C, class P, class B>
    QR_fast(TooN::Matrix<R,C,P,B> &m_)  :
        m(m_),tau(m.num_cols())
    {
        // Initialize tau vector
        tau = TooN::Zeros;

        // Factorize matrix
        compute();
    }

    void compute()
    {
        using TooN::Matrix;
        using TooN::Vector;

        for(int n = 0; n < m.num_cols(); ++n)
        {
            int sz = m.num_rows() - n;
            int nc = m.num_cols() - n;

            // Find H = I - tau*v*vT to put zeros below A(n,n)
            Matrix<TooN::Dynamic, TooN::Dynamic, Precision, typename Matrix<Rows,Cols,Precision>::SliceBase> s = m.slice(n,n,sz,nc);
            Precision normX = norm(s.T()[0]);
            Precision a = -(s[0][0] < 0?-1:1);
            Precision u_0 = s[0][0] - a*normX;

            TooN::Vector<TooN::Dynamic, Precision> v(sz);
            v = s.T()[0]/  u_0;
            v[0] = 1.0;
            s.T()[0].slice(1,sz-1).ref() = v.slice(1,sz-1);
            m[n][n] = a * normX;

            tau[n] = -a * u_0 / normX;

            // R = H*R
            pre_multiply_by_householder(s.slice(0, 1, sz, nc-1).ref(), v, tau[n]);
        }
    }

    TooN::Matrix<Cols,Cols,Precision> get_R()
    {
        TooN::Matrix<Cols,Cols,Precision> R = m.slice(0,0,m.num_cols(),m.num_cols());

        // Zero out the sub-diagonals
        for(int c = 0; c < R.num_cols(); ++c)
            for(int r = c+1; r < R.num_cols(); ++r)
                R[r][c] = 0.0;

        return R;
    }

    // Usually to get back the original matrix A by back-subbing Q into R, i.e. A = Q*R
    // Or, you can pass in an identity matrix to get Q itself, i.e. Q = Q1*Q2*Q3*...*Qt*I
    TooN::Matrix<Rows,Cols,Precision> backsubQ(TooN::Matrix<Rows,Cols,Precision> &X)
    {
        TooN::Matrix<Rows,Cols,Precision> A = X;

        for(int n = m.num_cols()-1; n>=0; --n)
        {
            int sz = m.num_rows() - n;

            TooN::Vector<> v(sz);
            v[0] = 1.0;
            v.slice(1, v.size()-1) = m.T()[n].slice(n+1, m.num_rows() - (n+1));

            pre_multiply_by_householder(A.slice(n,0,sz,m.num_cols()).ref(), v, tau[n]);
        }
        return A;
    }

    // Usually to solve least squares problem, by back-subbing into the error vector b
    // Ax = b
    // QRx = b
    // Rx = Q.T()*b
    TooN::Vector<Rows,Precision> backsubQT(const TooN::Vector<Rows,Precision> &b)
    {
        TooN::Vector<Rows,Precision> A = b;

        for(int n = 0; n < m.num_cols(); ++n)
        {
            int sz = m.num_rows() - n;

            TooN::Vector<> v(sz);
            v[0] = 1.0;
            v.slice(1, v.size()-1) = m.T()[n].slice(n+1, m.num_rows() - (n+1));

            pre_multiply_by_householder(A.slice(n,sz).ref(), v, tau[n]);
        }
        return A;
    }

private:

    template<class B1, class B2>
    void pre_multiply_by_householder(TooN::Matrix<TooN::Dynamic, TooN::Dynamic, Precision, B1>&A,
                                     const TooN::Vector<TooN::Dynamic, Precision, B2>&v,
                                     Precision tau)
    {
        for(int col=0; col < A.num_cols(); ++col)
        {
            Precision va = v * A.T()[col];

            for(int row=0; row < A.num_rows(); ++row)
                A[row][col] -= (tau*v[row]) * va;
        }
    }

    template<class B1, class B2>
    void pre_multiply_by_householder(TooN::Vector<TooN::Dynamic, Precision, B1>&A,
                                     const TooN::Vector<TooN::Dynamic, Precision, B2>&v,
                                     Precision tau)
    {
        Precision va = v * A;
        for(int i=0; i < 6; ++i)
        {
            A[i] -= (tau*v[i]) * va;
        }
    }

    TooN::Matrix<Rows,Cols> m;
    TooN::Vector<Cols> tau;
};

#endif // QR_FAST_H
