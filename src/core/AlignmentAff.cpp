// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "AlignmentAff.h"
#include "AlignAlg.h"
#include "timer.h"
#include <cvd/vision.h>
#include <cvd/image_interpolate.h>

AlignmentAff::
AlignmentAff()
{
}

AlignmentAff::
AlignmentAff(CVD::Image<CVD::byte> &refImB,
             CVD::Image<CVD::byte> &liveImB)
{
    my_data.im_ref = refImB;
    my_data.im_live = liveImB;

    my_data.gradient_im.resize(liveImB.size());
    CVD::gradient(my_data.im_ref, my_data.gradient_im);

    my_data.pred_im.resize(liveImB.size());
    my_data.pred_im.fill(0);

    m3 = TooN::Identity;
}

void
AlignmentAff::InitImage(int nLvl)
{
    my_data.pred_im.resize(my_data.im_live.size());
    my_data.pred_im.fill(0.0);
    my_data.num_data_points = 0;
}

bool
AlignmentAff::CreateDataPoint(CVD::ImageRef irPos, int nLvl)
{
    CVD::byte i = my_data.im_ref[irPos];
    TooN::Vector<2> v2grad = TooN::Zeros;
    v2grad[0] = my_data.gradient_im[irPos][0];
    v2grad[1] = my_data.gradient_im[irPos][1];
    my_data.vPixData.push_back(Data::PixelData(v2grad,irPos,i));
    my_data.num_data_points++;

    Data::PixelData &pd = my_data.vPixData.back();

    // Pre-compute jacobian
    TooN::Matrix<2,DIM> m2Dim = TooN::Zeros;
    m2Dim(0,0) = m2Dim(1,1) = irPos.x;
    m2Dim(0,2) = m2Dim(1,3) = irPos.y;
    m2Dim(0,4) = m2Dim(1,5) = 1.0;

    pd.vDimJacobian = pd.v2Gradient * m2Dim;

    return true;
}

TooN::Vector<AlignmentAff::DIM>
AlignmentAff::ComputeJtarget(int nIdx, TooN::Vector<2> v2grad, int nLvl)
{
    std::vector<Data::PixelData>::iterator it = my_data.vPixData.begin();
    std::advance(it,nIdx);
    Data::PixelData &pd = *it;

    TooN::Matrix<2,DIM> m2DimRef = TooN::Zeros;
    m2DimRef(0,0) = m2DimRef(1,1) = pd.v2Image[0];
    m2DimRef(0,2) = m2DimRef(1,3) = pd.v2Image[1];
    m2DimRef(0,4) = m2DimRef(1,5) = 1.0;

    TooN::Vector<DIM> result = v2grad * m2DimRef;

    return result;
}

TooN::Vector<3>
AlignmentAff::Project(const TooN::Vector<3> &v3)
{
    return m3 * v3;
}

CVD::byte
AlignmentAff::CreatePrediction(int nIdx, const TooN::Vector<3> &v3, int nLvl)
{
    std::vector<Data::PixelData>::iterator it = my_data.vPixData.begin();
    std::advance(it,nIdx);
    Data::PixelData &pd = *it;

    pd.v3Cam = v3;
    pd.v2Image = project(pd.v3Cam);

    CVD::image_interpolate<CVD::Interpolate::Bilinear, CVD::byte> interp(my_data.im_live);
    if(interp.in_image(pd.v2Image))
        pd.ucPredIntensity = interp[pd.v2Image];
    else
    {
        pd.ucPredIntensity = 0;
        pd.bNoData = true;
    }

    my_data.pred_im[pd.ir] = pd.ucPredIntensity;

    return pd.ucPredIntensity;
}

void
AlignmentAff::ComputeGradPredIm(CVD::Image<float[2]> &grad_pred_im, int nLvl)
{
    grad_pred_im.resize(my_data.pred_im.size());
    CVD::gradient(my_data.pred_im, grad_pred_im);
}

double
AlignmentAff::ComputeError(int nIdx, int nLvl)
{
    std::vector<Data::PixelData>::iterator it = my_data.vPixData.begin();
    std::advance(it,nIdx);
    Data::PixelData &pd = *it;

    if(pd.bNoData)
        pd.error = 0;
    else
        pd.error = ((double)(pd.ucPredIntensity - pd.ucIntensity))/255.0;

    return pd.error;
}

void
AlignmentAff::UpdateWarp(const TooN::Vector<DIM> &vDimUp)
{
    TooN::Matrix<3> m3up = AffWarpFromParams(-vDimUp);
    m3 = m3 * m3up;
}

TooN::Matrix<3>
AlignmentAff::AffWarpFromParams(const TooN::Vector<DIM> &vDimUp)
{
    TooN::Matrix<3> result;
    result[2] = TooN::makeVector(0,0,1);
    result(0,0) = 1.0 + vDimUp[0];
    result(1,0) = vDimUp[1];
    result(0,1) = vDimUp[2];
    result(1,1) = 1.0 + vDimUp[3];
    result(0,2) = vDimUp[4];
    result(1,2) = vDimUp[5];

    return result;
}

void
AlignmentAff::Align(std::string s, bool bReweight)
{
    // Initialize results
    result.vdTime.push_back(0.0);
    result.mat3_vec.push_back(m3);

    AlignAlg<AlignmentAff> *align_alg;
    align_alg = AlignAlg<AlignmentAff>::Create(s);
    align_alg->Init(*this);

    for(int k = 0; k < 20; ++k)
    {
        Timer timer;
        timer.start_timer();

        my_data.pred_im.fill(0.0);
        align_alg->Solve(*this, bReweight);

        // Log results
        timer.end_timer();
        result.vdTime.push_back(timer.get_time_mS());
        result.mat3_vec.push_back(m3);
    }

    delete align_alg;
}
