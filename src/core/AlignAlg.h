// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ALIGNALG_H
#define ALIGNALG_H

#include <TooN/TooN.h>
#include <TooN/Cholesky.h>
#include "../../thirdparty/MEstimator.h"
#include "AlignmentRGBD.h"

#include <cvd/glwindow.h>
#include <cvd/gl_helpers.h>

template<class Problem>
class AlignAlg
{
public:

    static AlignAlg<Problem>*
    Create(std::string s);

    virtual void
    Init(Problem &p, int nLvl=-1)=0;

    void
    InitPixData(Problem &p, int nLvl)
    {
        p.InitImage(nLvl);

        CVD::ImageRef irScan = CVD::ImageRef_zero;
        TooN::Matrix<Problem::DIM> mDimH = TooN::Zeros;
        do
        {
            if(p.CreateDataPoint(irScan,nLvl)==false)
                continue;

            TooN::Vector<Problem::DIM> vDimJ = p.GetJsource(p.GetNumDataPoints(nLvl)-1,nLvl);
            mDimH += vDimJ.as_col() * vDimJ.as_row();
        }while(irScan.next(p.GetImageSize(nLvl)));

        TooN::Cholesky<Problem::DIM> chol(mDimH);
        TooN::Matrix<Problem::DIM> mDimHInv = chol.get_inverse();
        p.SetHInv(mDimHInv, nLvl);
    }

    virtual void
    Solve(Problem &p, bool mbReweight, int nLvl=-1)=0;
};

template<class Problem>
class IC : public AlignAlg<Problem>
{
public:

    TooN::Vector<Problem::DIM> vDimAccum;
    TooN::Matrix<Problem::DIM,Problem::DIM> mDimH;

    IC(){}

    void
    Init(Problem &p, int nLvl=-1)
    {
        this->InitPixData(p, nLvl);
    }

    void
    Solve(Problem &p, bool mbReweight, int nLvl=-1)
    {
        CVD::Image<float> err_im(p.GetImageSize(nLvl));
        err_im.fill(0);

        std::vector<double> vdErrorSquared;
        vdErrorSquared.reserve(p.GetNumDataPoints(nLvl));
        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
        {
            TooN::Vector<3> v3Cam = p.Project(p.GetVec3Meas(i,nLvl));
            p.CreatePrediction(i, v3Cam, nLvl);
            double dErr = p.ComputeError(i,nLvl);

            vdErrorSquared.push_back(dErr*dErr);

            err_im[p.GetImPos(i,nLvl)] = fabs(dErr);
        };

//        CVD::GLWindow glw(p.GetImageSize(nLvl),"");
//        glDrawPixels(err_im);
//        glw.swap_buffers();
//        std::cin.get();

        double dSigmaSquared=1.0;
        if(mbReweight)
            dSigmaSquared = Huber::FindSigmaSquared(vdErrorSquared);

        // Accumulation
        vDimAccum = TooN::Zeros;
        mDimH = TooN::Zeros;
        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
        {
            if(p.bHasNoData(i,nLvl))
                continue;

            double dErr = p.GetError(i,nLvl);
            TooN::Vector<Problem::DIM> vDimJ = p.GetJsource(i,nLvl);
            if(mbReweight)
            {
                double dWeight = Huber::Weight(dErr*dErr, dSigmaSquared);
                double dSqrtWeight = sqrt(dWeight);
                vDimJ *= dSqrtWeight;
                dErr *= dSqrtWeight;

                mDimH += vDimJ.as_col() * vDimJ.as_row();
                vDimAccum += vDimJ * dErr;
            }
            else
                vDimAccum += vDimJ * dErr;
        };

        // Compute update
        TooN::Vector<Problem::DIM> vDimUp = TooN::Zeros;
        if(mbReweight)
        {
            TooN::Cholesky<Problem::DIM> cholmDimH(mDimH);
            vDimUp = cholmDimH.backsub(vDimAccum);
        }
        else
            vDimUp = p.GetHInv(nLvl) * vDimAccum;

        p.UpdateWarp(vDimUp);
    }
};

template<class Problem>
class ICprecond : public AlignAlg<Problem>
{
protected:

    class Precond
    {
    public:
        enum {DIM_ = Problem::DIM};

        virtual void
        Reset()=0;

        virtual void
        Accumulate(const TooN::Vector<DIM_> &v6,
                   const double dWeight)=0;

        virtual TooN::Vector<DIM_>
        Backsub(const TooN::Vector<DIM_> &vDim)=0;

    protected:

        TooN::Matrix<DIM_> mDimOffDiag;
        TooN::Vector<DIM_> vDimDiag;
    };

    class ScaledI: public Precond
    {
    public:

        ScaledI()
        {
            Precond::mDimOffDiag = TooN::Zeros;
            Precond::vDimDiag = TooN::Zeros;
            mdWeightSum = 0.0;
            nCount = 0;
        }

        void
        Reset()
        {
            Precond::mDimOffDiag = TooN::Zeros;
            Precond::vDimDiag = TooN::Zeros;
            mdWeightSum = 0.0;
            nCount = 0;
        }

        void
        Accumulate(const TooN::Vector<Precond::DIM_> &v6,
                   const double dWeight)
        {
            ++nCount;
            mdWeightSum += dWeight;
        }

        TooN::Vector<Precond::DIM_>
        Backsub(const TooN::Vector<Precond::DIM_> &vDim)
        {
            TooN::Vector<Precond::DIM_> vDimResult = vDim;
            double dInvAvgWeight = nCount / mdWeightSum;

            vDimResult *= dInvAvgWeight;
            return vDimResult;
        }

    private:
        double mdWeightSum;
        int nCount;
    };

    class Diag: public Precond
    {
    public:

        Diag()
        {
            Precond::mDimOffDiag = TooN::Zeros;
            Precond::vDimDiag = TooN::Zeros;
        }

        void
        Reset()
        {
            Precond::mDimOffDiag = TooN::Zeros;
            Precond::vDimDiag = TooN::Zeros;
        }

        void
        Accumulate(const TooN::Vector<Precond::DIM_> &vDim,
                   const double dWeight)
        {
            for(int i=0;i<Precond::DIM_;++i)
                Precond::vDimDiag[i] += vDim[i]*vDim[i];
        }

        TooN::Vector<Precond::DIM_>
        Backsub(const TooN::Vector<Precond::DIM_> &vDim)
        {
            TooN::Vector<Precond::DIM_> vDimResult;
            for(int i=0;i<Precond::DIM_;++i)
                vDimResult[i] = vDim[i] / Precond::vDimDiag[i];

            return vDimResult;
        }
    };

    class OffDiag: public Precond
    {
    public:
        OffDiag()
        {
            Precond::mDimOffDiag = TooN::Zeros;
            Precond::vDimDiag = TooN::Zeros;
        }

        void
        Reset()
        {
            Precond::mDimOffDiag = TooN::Zeros;
            Precond::vDimDiag = TooN::Zeros;
        }

        void
        Accumulate(const TooN::Vector<Precond::DIM_> &vDim,
                   const double dWeight)
        {
            // Only do upper triangle
            for(int c=0; c<Precond::DIM_; ++c)
            {
                Precond::vDimDiag[c] += vDim[c]*vDim[c];
                for(int r=0; r<c; ++r)
                    Precond::mDimOffDiag[r][c] += vDim[r]*vDim[c];
            }
        }

        TooN::Vector<Precond::DIM_>
        Backsub(const TooN::Vector<Precond::DIM_> &vDim)
        {
            TooN::Vector<Precond::DIM_> vDimSqrtLambdaInv;
            TooN::Matrix<Precond::DIM_> mDimDiagSqrtInv = TooN::Zeros;
            for(int i = 0; i < Precond::DIM_; ++i)
            {
                vDimSqrtLambdaInv[i] = 1.0 / sqrt(Precond::vDimDiag[i]);
                mDimDiagSqrtInv[i][i] = vDimSqrtLambdaInv[i];
            }

            // Fill in lower triangle and compute
            // approximate inverse:
            // (I+eps)^-1 ~= (I-eps) when
            // eps ~= 0
            TooN::Matrix<Precond::DIM_> mDimOffDiagSqrtInv = TooN::Zeros;
            for(int c = 0; c < Precond::DIM_; ++c)
            {
                mDimOffDiagSqrtInv[c][c] = 1.0;
                for(int r = c+1; r < Precond::DIM_; ++r)
                    mDimOffDiagSqrtInv(r,c) = mDimOffDiagSqrtInv(c,r) = -Precond::mDimOffDiag(c,r) * vDimSqrtLambdaInv[r] * vDimSqrtLambdaInv[c];
            }

            // Compute vector on the r.h.s
            TooN::Vector<Precond::DIM_> vDimresult = mDimDiagSqrtInv * (mDimOffDiagSqrtInv * (mDimDiagSqrtInv * vDim));

            return vDimresult;
        }
    };

    Precond *precond;

    ~ICprecond()
    {
        if(precond != NULL)
            delete precond;
    }

    TooN::Vector<Problem::DIM>
    BackSolve(TooN::Matrix<Problem::DIM> &mDimR,
              TooN::Vector<Problem::DIM> &vDim)
    {
        TooN::Vector<Problem::DIM> result = TooN::Zeros;
        for(int i=Problem::DIM-1; i>=0; --i)
        {
            result[i] = vDim[i];
            for(int j = i+1; j < Problem::DIM; ++j)
            {
                result[i] -= mDimR[i][j] * result[j];
            };
            result[i] /= mDimR[i][i];
        }

        return result;
    }

    void
    ComputeFullQR(Problem &p, int nLvl)
    {
        // Create the input matrix A
        // of the full data that has to be
        // fed into QR decomposition object
        const int num_rows = p.GetNumDataPoints(nLvl);

        TooN::Matrix<TooN::Dynamic,
                TooN::Dynamic,
                double,
                TooN::ColMajor> A(num_rows,Problem::DIM);
        A = TooN::Zeros;
        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
            A[i] = p.GetJsource(i, nLvl);
        QR_fast<> qrA(A);

        // Alright, computed the QR decomposition.
        // Now backsub result onto identity matrix
        // to get Q
        TooN::Matrix<> I(num_rows,Problem::DIM);
        I = TooN::Zeros;
        for(int c = 0; c < Problem::DIM; ++c)
            I[c][c] = 1.0;

        TooN::Matrix<> Q(num_rows,Problem::DIM);
        Q = qrA.backsubQ(I);
        TooN::Matrix<Problem::DIM> mDimR = qrA.get_R();
        p.SetR(mDimR, nLvl);

        // Fill in Q values
        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
            p.SetQrow(i, Q[i], nLvl);
    }

public:

    ICprecond(int nVariant)
    {
        precond = NULL;
        switch(nVariant)
        {
        case 0: {precond = new ScaledI(); break;}
        case 1: {precond = new Diag(); break;}
        case 2: {precond = new OffDiag(); break;}
        }
    }

    void
    Init(Problem &p, int nLvl=-1)
    {
        this->InitPixData(p, nLvl);
        ComputeFullQR(p, nLvl);
    }

    void Solve(Problem &p, bool mbReweight, int nLvl=-1)
    {
        precond->Reset();

        CVD::Image<float> err_im(p.GetImageSize(nLvl));
        err_im.fill(0);

        std::vector<double> vdErrorSquared;
        vdErrorSquared.reserve(p.GetNumDataPoints(nLvl));

        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
        {
            TooN::Vector<3> v3Cam = p.Project(p.GetVec3Meas(i,nLvl));
            p.CreatePrediction(i,v3Cam,nLvl);
            double dErr = p.ComputeError(i,nLvl);

            vdErrorSquared.push_back(dErr*dErr);
        }

        double dSigmaSquared = 1.0;
        if(mbReweight)
            dSigmaSquared = Huber::FindSigmaSquared(vdErrorSquared);

        // Accumulation:
        TooN::Vector<Problem::DIM> vDimAccum = TooN::Zeros;
        for(unsigned int i=0; i < p.GetNumDataPoints(nLvl); ++i)
        {
            if(p.bHasNoData(i,nLvl))
                continue;

            double dErr = p.GetError(i,nLvl);
            double dWeight = 1.0;
            if(mbReweight)
                dWeight = Huber::Weight(dErr*dErr, dSigmaSquared);

            TooN::Vector<Problem::DIM> vDimQ = p.GetQrow(i,nLvl);
            vDimAccum += vDimQ * dWeight * dErr;
            precond->Accumulate(vDimQ * sqrt(dWeight), dWeight);

            err_im[p.GetImPos(i,nLvl)] = fabs(dErr);
        }
//        CVD::GLWindow glw(p.GetImageSize(nLvl),"");
//        glDrawPixels(err_im);
//        glw.swap_buffers();
//        std::cin.get();

        // Compute update:
        TooN::Vector<Problem::DIM> vDimSlice = precond->Backsub(vDimAccum);
        TooN::Matrix<Problem::DIM> mDimR = p.GetR(nLvl);
        TooN::Vector<Problem::DIM> vDimUp = BackSolve(mDimR,vDimSlice);
        p.UpdateWarp(vDimUp);
    }
};

template<class Problem>
class ESMalg : public AlignAlg<Problem>
{
public:
    TooN::Vector<Problem::DIM> vDimAccum;
    TooN::Matrix<Problem::DIM> mDimH;

    ESMalg(){}

    void
    Init(Problem &p, int nLvl=-1)
    {
        this->InitPixData(p, nLvl);
    }

    void
    Solve(Problem &p, bool mbReweight, int nLvl=-1)
    {
        CVD::Image<float> err_im(p.GetImageSize(nLvl));
        err_im.fill(0);

        std::vector<double> vdErrorSquared;
        vdErrorSquared.reserve(p.GetNumDataPoints(nLvl));

        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
        {
            TooN::Vector<3> v3Cam = p.Project(p.GetVec3Meas(i,nLvl));
            p.CreatePrediction(i, v3Cam, nLvl);
            double dErr = p.ComputeError(i,nLvl);

            vdErrorSquared.push_back(dErr*dErr);

            err_im[p.GetImPos(i,nLvl)] = fabs(dErr);
        }
//        CVD::GLWindow glw(p.GetImageSize(nLvl),"");
//        glDrawPixels(err_im);
//        glw.swap_buffers();
//        std::cin.get();


        double dSigmaSquared = 1.0;
        if(mbReweight)
            dSigmaSquared = Huber::FindSigmaSquared(vdErrorSquared);

        // Compute jacobians:
        CVD::Image<float[2]> grad_pred_im;
        p.ComputeGradPredIm(grad_pred_im,nLvl);

        vDimAccum = TooN::Zeros;
        mDimH = TooN::Zeros;
        for(unsigned int i = 0; i < p.GetNumDataPoints(nLvl); ++i)
        {
            CVD::ImageRef irPos = p.GetImPos(i,nLvl);
            TooN::Vector<2> v2grad_ref = TooN::Zeros;
            v2grad_ref[0] = grad_pred_im[irPos][0];
            v2grad_ref[1] = grad_pred_im[irPos][1];

            if(p.bHasNoData(i,nLvl))
                continue;
            double dErr = p.GetError(i,nLvl);
            TooN::Vector<Problem::DIM> vDimJtemp = p.GetJsource(i,nLvl);
            TooN::Vector<Problem::DIM> vDimJref = p.ComputeJtarget(i,v2grad_ref,nLvl);
            TooN::Vector<Problem::DIM> vDimJ = 0.5* (vDimJtemp + vDimJref);

            if(mbReweight)
            {
                double dSqrtWeight = Huber::SquareRootWeight(dErr*dErr, dSigmaSquared);
                dErr *= dSqrtWeight;
                vDimJ *= dSqrtWeight;
            }

            mDimH += vDimJ.as_col() * vDimJ.as_row();
            vDimAccum += vDimJ * dErr;
        }

        // Compute updates:
        TooN::Cholesky<Problem::DIM> cholmDimH(mDimH);
        TooN::Vector<Problem::DIM> vDimUp = cholmDimH.backsub(vDimAccum);
        p.UpdateWarp(vDimUp);
    }
};

// Factory method
template<class Problem>
AlignAlg<Problem>*
AlignAlg<Problem>::Create(std::string s)
{
    if(s=="default")
        return new IC<Problem>();
    else if(s=="scaled_identity")
        return new ICprecond<Problem>(0);
    else if(s=="diagonal")
        return new ICprecond<Problem>(1);
    else if(s=="off_diagonal")
        return new ICprecond<Problem>(2);
    else if(s=="esm")
        return new ESMalg<Problem>();
}

#endif // ALIGNALG_H
