// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ALIGNMENTAFF_H
#define ALIGNMENTAFF_H

#include <cvd/image.h>
#include <cvd/byte.h>
#include <TooN/TooN.h>

struct ResultAff
{
    std::vector<TooN::Matrix<3> > mat3_vec;
    std::vector<double> vdTime;
};

class AlignmentAff
{
public:
    enum { DIM=6 };
protected:

    TooN::Matrix<3>
    AffWarpFromParams(const TooN::Vector<DIM> &vDimUp);

    struct Data
    {
        int num_data_points;
        CVD::Image<CVD::byte> im_ref;
        CVD::Image<CVD::byte> im_live;
        CVD::Image<CVD::byte> pred_im;
        CVD::Image<float[2]> gradient_im;

        struct PixelData
        {
            PixelData();

            PixelData(TooN::Vector<2> v2,
                      CVD::ImageRef irLoc,
                      CVD::byte intensity)
                : v2Gradient(v2),ir(irLoc),ucIntensity(intensity)
            {
                bNoData=false;
            }

            bool bNoData;
            CVD::ImageRef ir;
            CVD::byte ucIntensity;
            TooN::Vector<2> v2Gradient;

            // Intermediates:
            TooN::Vector<3> v3Cam;
            TooN::Vector<2> v2Image;
            CVD::byte ucPredIntensity;
            double error;

            // Row of Jacobian and Q factor
            TooN::Matrix<2,DIM> m2Dim;
            TooN::Vector<DIM> vDimJacobian;
            TooN::Vector<DIM> vDimQ;
        };

        TooN::Matrix<DIM> mDimR;
        TooN::Matrix<DIM> mDimHInv;
        std::vector<PixelData> vPixData;
    };
    Data my_data;
    TooN::Matrix<3> m3;
    ResultAff result;

public:

    AlignmentAff(CVD::Image<CVD::byte> &refImB,
                 CVD::Image<CVD::byte> &liveImB);

    AlignmentAff();

    void
    Align(std::string s, bool bReweight);

    inline ResultAff
    DumpResult(){return result;}

    void
    InitImage(int nLvl=-1);

    bool
    CreateDataPoint(CVD::ImageRef irPos, int nLvl=-1);

    TooN::Vector<DIM>
    ComputeJtarget(int nIdx, TooN::Vector<2> v2grad, int nLvl=-1);

    TooN::Vector<3>
    Project(const TooN::Vector<3> &v3);

    CVD::byte
    CreatePrediction(int nIdx, const TooN::Vector<3> &v3, int nLvl=-1);

    void
    ComputeGradPredIm(CVD::Image<float[2]> &grad_pred_im, int nLvl=-1);

    double
    ComputeError(int nIdx, int nLvl=-1);

    void
    UpdateWarp(const TooN::Vector<DIM> &vDimUp);

    // Getters and setters
    inline const CVD::ImageRef
    GetImageSize(int nLvl=-1) const
    {
        return my_data.im_live.size();
    }

    inline const int
    GetNumDataPoints(int nLvl=-1) const
    {
        return my_data.num_data_points;
    }

    inline const bool
    bHasNoData(int nIdx, int nLvl=-1) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data.vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.bNoData;
    }

    inline const CVD::ImageRef
    GetImPos(int nIdx, int nLvl=-1) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data.vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.ir;
    }

    inline const TooN::Vector<DIM>
    GetJsource(int nIdx, int nLvl=-1) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data.vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.vDimJacobian;
    }

    inline void
    SetQrow(int nIdx, const TooN::Vector<DIM> &vDim, int nLvl=-1)
    {
        std::vector<Data::PixelData>::iterator it = my_data.vPixData.begin() + nIdx;
        Data::PixelData &pd = *it;
        pd.vDimQ = vDim;
    }

    inline const TooN::Vector<DIM>
    GetQrow(int nIdx, int nLvl=-1) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data.vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.vDimQ;
    }

    inline const TooN::Vector<3>
    GetVec3Meas(int nIdx, int nLvl=-1) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data.vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;

        TooN::Vector<3> result;
        result = TooN::makeVector(pd.ir.x, pd.ir.y, 1.0);
        return result;
    }

    inline const double
    GetError(int nIdx, int nLvl=-1) const
    {
        std::vector<Data::PixelData>::const_iterator it = my_data.vPixData.begin() + nIdx;
        const Data::PixelData &pd = *it;
        return pd.error;
    }

    inline void
    SetHInv(TooN::Matrix<DIM> &mDimHInv, int nLvl)
    {
        my_data.mDimHInv = mDimHInv;
    }

    inline const TooN::Matrix<DIM>
    GetHInv(int nLvl) const
    {
        return my_data.mDimHInv;
    }

    inline void
    SetR(TooN::Matrix<DIM> &mDimR, int nLvl)
    {
        my_data.mDimR = mDimR;
    }

    inline const TooN::Matrix<DIM>
    GetR(int nLvl) const
    {
        return my_data.mDimR;
    }
};

#endif // ALIGNMENTAFF_H
