// Copyright (C) 2015 Vincent Lui, (vincent.lui@monash.edu), Monash University
// 
// This file is part of FastIC.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TIMER_H
#define TIMER_H

#endif // TIMER_H

#include <sys/time.h>

class Timer
{
private:
    struct timeval start_time;
    struct timeval end_time;
    double dStart;
    double dEnd;

public:
    Timer() :
        dStart(0),
        dEnd(0)
    {}

    void start_timer()
    {
        gettimeofday(&start_time, NULL);
    }

    void end_timer()
    {
        gettimeofday(&end_time, NULL);
        dStart = start_time.tv_sec * 1000000 + start_time.tv_usec;
        dEnd = end_time.tv_sec * 1000000 + end_time.tv_usec;
    }

    double get_time_mS()
    {
        return (dEnd - dStart) / 1000;
    }
};
